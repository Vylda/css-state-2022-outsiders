import './css/index.css';
import { StrictMode } from 'react';
import { createRoot } from 'react-dom/client';
import {
  createHashRouter,
  RouterProvider,
} from 'react-router-dom';
import App from './App';
import pages from './pages/pages';

const router = createHashRouter(pages.map((pageItem) => {
  const { path, errorElement } = pageItem;
  return {
    path,
    element: <App />,
    errorElement,
  };
}));

const root = createRoot(document.querySelector('#app'));
root.render(
  <StrictMode>
    <RouterProvider router={router} />
  </StrictMode>,
);
