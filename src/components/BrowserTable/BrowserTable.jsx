import {
  Box, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow,
} from '@mui/material';
import clsx from 'clsx';
import { PropTypes } from 'prop-types';
import style from './BrowserTable.module.css';
import releases from '../../data/releases';

const resolveVersion = (version) => {
  if (version === undefined) {
    return '-';
  }
  if (typeof version === 'number') {
    return version.toString();
  }

  const { partial, version: ver } = version;

  return (
    <span
      className={clsx(partial && style.partial)}
      title="Částečná podpora"
    >
      {ver.toString()}
    </span>
  );
};

const getReleaseDate = (type, ver) => {
  const versions = releases[type];
  if (!versions) {
    console.error(`Type ${type} is missing in releases`);
    return undefined;
  }

  const solvedVer = typeof ver === 'number' ? ver : ver.version;

  const version = versions.get(solvedVer);
  if (!version || version.length < 3) {
    console.error(`Version ${version} is missing in releases`);
    return undefined;
  }

  const [y, m, d, isOldest] = version;

  return {
    releaseDate: (new Date(y, m, d)).toLocaleDateString(),
    isOldest: !!isOldest,
  };
};

const getReleaseDates = (chrome, edge, firefox, opera, safari) => ({
  chrome: getReleaseDate('chrome', chrome),
  edge: getReleaseDate('edge', edge),
  firefox: getReleaseDate('firefox', firefox),
  opera: getReleaseDate('opera', opera),
  safari: getReleaseDate('safari', safari),
});

const BrowserTable = ({
  chrome, edge, firefox, opera, safari, title,
}) => {
  const rels = getReleaseDates(chrome, edge, firefox, opera, safari);

  return (!chrome && !edge && !firefox && !opera && !safari
    ? null
    : (
      <Box className={style.box}>
        <TableContainer component={Paper} className={style.table}>
          <Table size="small" aria-label={`Browser compatibility for ${title}`}>
            <TableHead>
              <TableRow>
                <TableCell align="center">Chrome</TableCell>
                <TableCell align="center">Edge</TableCell>
                <TableCell align="center">Firefox</TableCell>
                <TableCell align="center">Opera</TableCell>
                <TableCell align="center">Safari</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              <TableRow>
                <TableCell
                  align="center"
                  className={clsx(
                    !!rels.chrome?.isOldest && style.oldest,
                    !rels.chrome && style.missing,
                  )}
                  title={`${(!!rels.chrome?.isOldest && 'Nejstarší, ') || ''}${rels.chrome?.releaseDate || ''}`}
                >
                  {resolveVersion(chrome)}
                </TableCell>
                <TableCell
                  align="center"
                  className={clsx(
                    !!rels.edge?.isOldest && style.oldest,
                    !rels.edge && style.missing,
                  )}
                  title={`${(!!rels.edge?.isOldest && 'Nejstarší, ') || ''}${rels.edge?.releaseDate || ''}`}
                >
                  {resolveVersion(edge)}

                </TableCell>
                <TableCell
                  align="center"
                  className={clsx(
                    !!rels.firefox?.isOldest && style.oldest,
                    !rels.firefox && style.missing,
                  )}
                  title={`${(!!rels.firefox?.isOldest && 'Nejstarší, ') || ''}${rels.firefox?.releaseDate || ''}`}
                >
                  {resolveVersion(firefox)}

                </TableCell>
                <TableCell
                  align="center"
                  className={clsx(
                    !!rels.opera?.isOldest && style.oldest,
                    !rels.opera && style.missing,
                  )}
                  title={`${(!!rels.opera?.isOldest && 'Nejstarší, ') || ''}${rels.opera?.releaseDate || ''}`}
                >
                  {resolveVersion(opera)}

                </TableCell>
                <TableCell
                  align="center"
                  className={clsx(
                    !!rels.safari?.isOldest && style.oldest,
                    !rels.safari && style.missing,
                  )}
                  title={`${(!!rels.safari?.isOldest && 'Nejstarší, ') || ''}${rels.safari?.releaseDate || ''}`}
                >
                  {resolveVersion(safari)}

                </TableCell>
              </TableRow>
            </TableBody>
          </Table>
        </TableContainer>
      </Box>
    ));
};
export const version = PropTypes.oneOfType([
  PropTypes.number,
  PropTypes.shape({
    version: PropTypes.number,
    partial: PropTypes.bool,
  }),
]);

BrowserTable.propTypes = {
  chrome: version,
  edge: version,
  firefox: version,
  opera: version,
  safari: version,
  title: PropTypes.string.isRequired,
};

export default BrowserTable;
