import { ChevronLeft, ChevronRight } from '@mui/icons-material';
import { PropTypes } from 'prop-types';
import style from './Footer.module.css';
import ButtonLink from '../ButtonLink/ButtonLink';

const Footer = ({ next, prev }) => (
  <footer className={style.footer}>
    <ButtonLink
      url={prev}
      size="small"
      className={style.button}
      color="info"
      startIcon={<ChevronLeft />}
      variant="outlined"
    >
      Předchozí
    </ButtonLink>
    <span>&copy; 2023 Vylda</span>
    <ButtonLink
      endIcon={<ChevronRight />}
      color="info"
      className={style.button}
      size="small"
      url={next}
      variant="outlined"
    >
      Následující
    </ButtonLink>
  </footer>
);

Footer.propTypes = {
  next: PropTypes.string,
  prev: PropTypes.string,
};

export default Footer;
