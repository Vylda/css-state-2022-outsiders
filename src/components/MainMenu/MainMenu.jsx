import Button from '@mui/material/Button';
import Menu from '@mui/material/Menu';
import MenuList from '@mui/material/MenuList';
import { PropTypes } from 'prop-types';
import { useMemo, useState } from 'react';
import MainMenuItem from './components/MainMenuItem';
import pages from '../../pages/pages';

const MainMenu = ({ path = '/' }) => {
  const [anchorEl, setAnchorEl] = useState();

  const open = !!anchorEl;

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(undefined);
  };

  const items = useMemo(() => pages.map((pageItem) => {
    const { path: pagePath, title } = pageItem;
    return {
      path: pagePath.startsWith('/') ? pagePath : `/${pagePath}`,
      title,
    };
  }), [pages]);

  return (
    <>
      <Button
        id="demo-positioned-button"
        aria-controls={open ? 'demo-positioned-menu' : undefined}
        aria-haspopup="true"
        aria-expanded={open ? 'true' : undefined}
        onClick={handleClick}
      >
        Navigace
      </Button>
      <Menu
        id="demo-positioned-menu"
        aria-labelledby="demo-positioned-button"
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'left',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'left',
        }}
      >
        <MenuList dense>
          {items.map((item) => (
            <MainMenuItem
              key={item.path}
              checked={item.path === path}
              close={() => setAnchorEl()}
              item={item}
            />
          ))}
        </MenuList>
      </Menu>
    </>
  );
};

MainMenu.propTypes = {
  path: PropTypes.string,
};

export default MainMenu;
