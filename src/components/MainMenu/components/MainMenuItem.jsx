import Check from '@mui/icons-material/Check';
import ListItemIcon from '@mui/material/ListItemIcon';
import MenuItem from '@mui/material/MenuItem';
import { PropTypes } from 'prop-types';
import { NavLink } from 'react-router-dom';
import style from './MainMenuItem.module.css';

const MainMenuItem = ({ checked = false, close, item }) => (
  <NavLink
    to={item.path}
    onClick={close}
    className={style.link}
  >
    <MenuItem>
      {item.title}
      {checked && (
        <ListItemIcon className={style.icon}>
          <Check />
        </ListItemIcon>
      )}
    </MenuItem>
  </NavLink>
);

MainMenuItem.propTypes = {
  checked: PropTypes.bool,
  close: PropTypes.func.isRequired,
  item: PropTypes.shape({
    path: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
  }).isRequired,
};

export default MainMenuItem;
