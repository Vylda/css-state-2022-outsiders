import { Box, Paper, Typography } from '@mui/material';
import { PropTypes } from 'prop-types';
import { dataFormatter } from 'Utils/formatters';
import style from './Reference.module.css';
import basicData from '../../data/state.json';
import BrowserTable, { version } from '../BrowserTable/BrowserTable';
import ButtonLink from '../ButtonLink/ButtonLink';

const Reference = ({
  canIUseUrl,
  children,
  chrome,
  edge,
  firefox,
  statsName,
  mdnUrl,
  opera,
  safari,
  title,
}) => {
  let pct;
  if (statsName) {
    const searched = basicData.find((item) => item.name === statsName);
    if (searched) {
      pct = dataFormatter(searched.iDontKnow);
    }
  }

  return (
    <Paper elevation={3} className={style['top-paper']}>
      <Box className={style.reference}>

        <Typography variant="h6" className={style['buttons-title']}>
          {title}
          <span title="Nezná to" className={style.percentages}>
            {!!pct && ` (${pct})`}
          </span>
        </Typography>

        <Box className={style['reference-buttons']}>
          <ButtonLink
            blank
            color="info"
            size="small"
            url={mdnUrl}
            variant="contained"
          >
            MDN
          </ButtonLink>
          <ButtonLink
            blank
            color="info"
            size="small"
            url={canIUseUrl}
            variant="contained"
          >
            CanIUse
          </ButtonLink>
        </Box>
        <BrowserTable
          chrome={chrome}
          edge={edge}
          firefox={firefox}
          opera={opera}
          safari={safari}
          title={title}
        />
      </Box>

      <Box className={style['property-info']}>
        {children}
      </Box>
    </Paper>
  );
};
Reference.propTypes = {
  canIUseUrl: PropTypes.string.isRequired,
  children: PropTypes.node,
  chrome: version,
  edge: version,
  firefox: version,
  statsName: PropTypes.string,
  mdnUrl: PropTypes.string.isRequired,
  opera: version,
  safari: version,
  title: PropTypes.string.isRequired,
};

export default Reference;
