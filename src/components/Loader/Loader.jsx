import { Paper } from '@mui/material';
import { useEffect, useRef, useState } from 'react';
import style from './Loader.module.css';

const Loader = () => {
  const [prgState, setPrgState] = useState(0);
  const prgStateRef = useRef(0);

  useEffect(() => {
    setInterval(() => {
      const newValue = prgStateRef.current + 1;
      if (newValue > 100) {
        prgStateRef.current = 0;
        setPrgState(0);
        return;
      }

      prgStateRef.current = newValue;
      setPrgState(newValue);
    }, 100);
  }, []);

  return (
    <div>
      <Paper className={style.paper} elevation={3}>
        <progress className={style.progress} max="100" value={prgState} />
        <br />
        Nahrávám…
      </Paper>
    </div>
  );
};

export default Loader;
