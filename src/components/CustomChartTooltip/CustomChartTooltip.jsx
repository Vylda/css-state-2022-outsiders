import { CheckCircle, Error } from '@mui/icons-material';
import { Card, CardContent } from '@mui/material';
import { dataFormatter } from 'Utils/formatters';
import style from './CustomChartTooltip.module.css';

/* eslint-disable react/prop-types */
const CustomChartTooltip = ({ active, payload }) => {
  if (active && payload?.length) {
    const { payload: { plainName, group, fullBrowserSupport } } = payload[0];

    return (
      <Card elevation={5} className={style.tooltip}>
        <CardContent className={style.content}>
          <h1 className={style.header}>
            {plainName}
            { fullBrowserSupport
              ? <CheckCircle color="success" className={style.icon} />
              : <Error color="error" className={style.icon} />}
          </h1>
          <h2 className={style.subheader}>
            {group}
          </h2>
          <div>
            {payload.map((payloadItem) => (
              <p
                key={`${payloadItem.fill}${payloadItem.name}`}
                style={{ color: payloadItem.fill }}
                className={style.data}
              >
                {`${payloadItem.name}: `}
                <span className={style.value}>{dataFormatter(payloadItem.value)}</span>
              </p>
            ))}
          </div>
        </CardContent>
      </Card>
    );
  }

  return null;
};

export default CustomChartTooltip;
