import FiberManualRecordIcon from '@mui/icons-material/FiberManualRecord';
import { Button } from '@mui/material';
import { PropTypes } from 'prop-types';

const CodeButton = ({
  children,
  hasStartIcon = true,
  onClick = () => {},
  showStartIcon = false,
}) => (
  <Button
    onClick={onClick}
    startIcon={hasStartIcon && (
      <FiberManualRecordIcon
        sx={showStartIcon ? undefined : { color: 'transparent' }}
      />
    )}
  >
    {children}
  </Button>
);

CodeButton.propTypes = {
  children: PropTypes.element.isRequired,
  hasStartIcon: PropTypes.bool,
  onClick: PropTypes.func,
  showStartIcon: PropTypes.bool,
};

export default CodeButton;
