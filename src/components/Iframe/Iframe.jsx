import { PropTypes } from 'prop-types';
import { useEffect, useRef } from 'react';
import style from './Iframe.module.css';

const Iframe = ({ src }) => {
  const iframeRef = useRef();
  useEffect(() => {
    const frame = iframeRef.current;

    const adjustHeight = () => {
      const { bottom } = frame.contentWindow.document.body.getBoundingClientRect();
      frame.style.height = `${bottom}px`;
    };

    frame.addEventListener('load', adjustHeight);

    return () => {
      frame.removeEventListener('load', adjustHeight);
    };
  }, []);

  return (
    <iframe
      ref={iframeRef}
      className={style.iframe}
      src={src}
      height="100"
      title="custom element"
    />
  );
};

Iframe.propTypes = {
  src: PropTypes.string.isRequired,
};

export default Iframe;
