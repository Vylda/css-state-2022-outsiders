import LabelOutlinedIcon from '@mui/icons-material/LabelOutlined';
import {
  List, ListItem, ListItemIcon, ListItemText, ListSubheader,
} from '@mui/material';
import PropTypes from 'prop-types';

const ValuesList = ({ title = 'Možné hodnoty:', list }) => (
  <List dense disablePadding>
    <ListSubheader>{title}</ListSubheader>
    {list.map((listItem) => {
      const { id, primary, secondary } = listItem;
      return (
        <ListItem sx={{ paddingTop: 0, paddingBottom: 0 }} key={id}>
          <ListItemIcon>
            <LabelOutlinedIcon />
          </ListItemIcon>
          <ListItemText
            primary={primary}
            secondary={secondary}
          />
        </ListItem>
      );
    })}
  </List>
);

ValuesList.propTypes = {
  title: PropTypes.string,
  list: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired,
    primary: PropTypes.node.isRequired,
    secondary: PropTypes.node,
  })).isRequired,
};
export default ValuesList;
