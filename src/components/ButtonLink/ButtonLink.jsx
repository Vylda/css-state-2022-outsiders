/* eslint-disable react/forbid-prop-types */
import { Button } from '@mui/material';
import { PropTypes } from 'prop-types';
import { useMemo } from 'react';
import { Link } from 'react-router-dom';

const ButtonLink = ({
  blank = false, children, className, color, endIcon, size, startIcon, url, variant,
}) => {
  const disabled = useMemo(() => !url);

  return (
    <Button
      className={className}
      color={color}
      component={Link}
      disabled={disabled}
      endIcon={endIcon}
      size={size}
      startIcon={startIcon}
      to={url}
      variant={variant}
      target={blank ? '_blank' : undefined}
    >
      {children}
    </Button>
  );
};

ButtonLink.propTypes = {
  blank: PropTypes.bool,
  children: PropTypes.node,
  className: PropTypes.string,
  color: PropTypes.oneOf(['inherit', 'primary', 'secondary', 'success', 'error', 'info', 'warning']),
  endIcon: PropTypes.node,
  size: PropTypes.oneOf(['small', 'medium', 'large']),
  startIcon: PropTypes.node,
  url: PropTypes.string,
  variant: PropTypes.oneOf(['text', 'outlined', 'contained']),
};

export default ButtonLink;
