import { lazy } from 'react';
import ErrorPage from './ErrorPage';
import Index from './Index';

const Stats = lazy(() => import('./Stats'));
const FontPalette = lazy(() => import('./FontPalette'));
const ShadowDomPartA = lazy(() => import('./ShadowDomPartA'));
const ShadowDomPartB = lazy(() => import('./ShadowDomPartB'));
const ShadowDomPartC = lazy(() => import('./ShadowDomPartC'));
const PrefersContrast = lazy(() => import('./PrefersContrast'));
const ColorScheme = lazy(() => import('./ColorScheme'));
const CascadeLayers = lazy(() => import('./CascadeLayers'));
const ImageSet = lazy(() => import('./ImageSet'));
const OverscrollBehavior = lazy(() => import('./OverscrollBehavior'));
const AccentColor = lazy(() => import('./AccentColor'));
const WillChange = lazy(() => import('./WillChange'));
const LineClamp = lazy(() => import('./LineClamp'));
const ConicGradient = lazy(() => import('./ConicGradient'));
const WritingModes = lazy(() => import('./WritingModes'));
const FontDisplay = lazy(() => import('./FontDisplay'));
const CssLogicalProps = lazy(() => import('./CssLogicalProps'));
const Where = lazy(() => import('./Where'));
const Thanks = lazy(() => import('./Thanks'));

const pages = [
  {
    path: '/',
    errorElement: <ErrorPage />,
    component: <Index />,
    title: 'Úvod',
  },
  {
    path: 'stats/',
    component: <Stats />,
    title: 'Statistiky',
  },
  {
    path: 'font-palette/',
    component: <FontPalette />,
    title: 'font-palette',
  },
  {
    path: 'shadow-dom-part-komponenta/',
    component: <ShadowDomPartA />,
    title: 'Shadow DOM ::part() - komponenta',
  },
  {
    path: 'shadow-dom-part-pouziti/',
    component: <ShadowDomPartB />,
    title: 'Shadow DOM ::part() - použití komponenty',
  },
  {
    path: 'shadow-dom-part-pseudoelement/',
    component: <ShadowDomPartC />,
    title: 'Shadow DOM ::part() - řešení',
  },
  {
    path: 'prefers-contrast/',
    component: <PrefersContrast />,
    title: 'prefers-contrast',
  },
  {
    path: 'color-scheme/',
    component: <ColorScheme />,
    title: 'color-scheme',
  },
  {
    path: 'cascade-layers/',
    component: <CascadeLayers />,
    title: 'Cascade layers',
  },
  {
    path: 'image-set/',
    component: <ImageSet />,
    title: 'image-set()',
  },
  {
    path: 'overscroll-behavior/',
    component: <OverscrollBehavior />,
    title: 'overscroll-behavior',
  },
  {
    path: 'accent-color/',
    component: <AccentColor />,
    title: 'accent-color',
  },
  {
    path: 'will-change/',
    component: <WillChange />,
    title: 'will-change',
  },
  {
    path: 'line-clamp/',
    component: <LineClamp />,
    title: 'line-clamp',
  },
  {
    path: 'conic-gradient/',
    component: <ConicGradient />,
    title: 'conic-gradient()',
  },
  {
    path: 'writing-modes/',
    component: <WritingModes />,
    title: 'writing modes',
  },
  {
    path: 'font-display/',
    component: <FontDisplay />,
    title: 'font-display',
  },
  {
    path: 'css-logical-properties/',
    component: <CssLogicalProps />,
    title: 'CSS logical properties',
  },
  {
    path: 'where/',
    component: <Where />,
    title: ':where()',
  },
  {
    path: 'thanks/',
    component: <Thanks />,
    title: 'konec',
  },
];

export default pages;
