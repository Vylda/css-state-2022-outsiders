import { useEffect, useState } from 'react';
import basicData from '../../data/state.json';

const resolveData = (alldata) => alldata.map((dataItem) => {
  const {
    group, name, iDontKnow, used, fullBrowserSupport,
  } = dataItem;

  // eslint-disable-next-line no-unused-vars, no-irregular-whitespace
  const resolvedName = `${fullBrowserSupport ? '+' : '−'} ${name}`;
  return {
    group,
    plainName: name,
    fullBrowserSupport,
    name: resolvedName,
    'Neznám to': iDontKnow,
    'Slyšel jsem': 1 - iDontKnow - used,
    'Použil jsem': used,
  };
});

const sortData = (data) => data.sort(
  (current, next) => next.iDontKnow - current.iDontKnow,
);
const sortDataUsed = (data) => data.sort(
  (current, next) => next.used - current.used,
);

const filterData = (data) => data.filter((item) => item.fullBrowserSupport);

const useStats = () => {
  const [data, setData] = useState({
    data: [],
    title: '',
    subtitle: '',
  });

  const [dataState, setDataState] = useState('all');

  useEffect(() => {
    switch (dataState) {
      case 'all': {
        const resolvedData = resolveData([...basicData]);
        setData({
          data: resolvedData,
          title: 'Všechny vlastnosti',
          subtitle: `Jak dopadly veškeré otázky na vlastnosti CSS ve State of CSS 2022; řazeno přesně podle webu (celkový počet: ${resolvedData.length}).`,
        });
        break;
      }
      case 'byIDN': {
        const sortedData = sortData([...basicData]);
        const resolvedData = resolveData(sortedData);
        setData({
          data: resolvedData,
          title: 'Seřazené vlastnosti',
          subtitle: `Vlastnosti seřazené podle míry neznalosti (celkový počet: ${resolvedData.length}).`,
        });
        break;
      }
      case 'justWithFullSupport': {
        const sortedData = sortData([...basicData]);
        const filteredData = filterData(sortedData);
        const resolvedData = resolveData(filteredData);
        setData({
          data: resolvedData,
          title: 'Seřazené vlastnosti s podporou',
          subtitle: `Vlastnosti, které mají podporu ve všech základních prohlížečích (Chrome, Firefox, Safari a Edge), seřazené podle míry neznalosti (celkový počet: ${resolvedData.length}).`,
        });
        break;
      }
      case 'supportedSelect': {
        const sortedData = sortData([...basicData]);
        const filteredData = filterData(sortedData).slice(0, 15);
        const resolvedData = resolveData(filteredData);
        setData({
          data: resolvedData,
          title: 'Vlastnosti, které nás budou zajímat',
          subtitle: `Vlastnosti, které mají podporu ve všech základních prohlížečích (Chrome, Firefox, Safari a Edge), seřazené podle míry neznalosti a na které se tady podíváme (celkový počet: ${resolvedData.length}).`,
        });
        break;
      }
      case 'used': {
        const sortedData = sortDataUsed([...basicData]);
        const resolvedData = resolveData(sortedData);
        setData({
          data: resolvedData,
          title: 'Používané vlastnosti',
          subtitle: `Vlastnosti seřazené podle používání (celkový počet: ${resolvedData.length}).`,
        });
        break;
      }
      default:
    }
  }, [dataState]);

  const onChange = (event, newValue) => {
    setDataState(newValue);
  };

  return {
    data: data.data,
    dataState,
    onChange,
    subtitle: data.subtitle,
    title: data.title,
  };
};

export default useStats;
