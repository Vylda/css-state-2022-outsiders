import { Box, Typography } from '@mui/material';
import clsx from 'clsx';
import { Helmet } from 'react-helmet';
import SyntaxHighlighter from 'react-syntax-highlighter';
import { monokaiSublime } from 'react-syntax-highlighter/dist/esm/styles/hljs';
import { Iframe, Reference } from 'Components';
import basicStyle from './BasicStyles.module.css';

const ShadowDomPartC = () => (
  <>
    <Helmet>
      <title>Shadow DOM ::part() - řešení | State of CSS 2022 outsiders</title>
    </Helmet>
    <h1>Shadow DOM ::part() - řešení</h1>
    <Reference
      canIUseUrl="https://caniuse.com/mdn-css_selectors_part"
      mdnUrl="https://developer.mozilla.org/en-US/docs/Web/CSS/::part"
      chrome={73}
      edge={79}
      firefox={72}
      statsName="shadow DOM ::part()"
      opera={60}
      safari={13.1}
      title="Pseudoelement ::part"
    >
      <p>
        Pseudoelement
        {' '}
        <code>::part</code>
        {' '}
        vleze do komponenty - poruší vlastnosti Shadow DOMu.
      </p>
    </Reference>
    <Box>
      <Box className={clsx(basicStyle.code, basicStyle['two-cols'])}>
        <Box>
          <Typography variant="h5">CSS komponenty</Typography>
          <SyntaxHighlighter language="css" style={monokaiSublime} showLineNumbers>
            {`*::part(day) {
  box-sizing: content-box;
  padding: 1rem;
  border-bottom: 2px solid transparent;
  cursor: pointer;
  width: 5rem;
  text-align: center;
}

*::part(day):hover {
  background-color: rgb(230, 231, 240);
}

*::part(today) {
  font-weight: bold;
  border-color: rgba(25, 25, 245, 0.692);
}`}
          </SyntaxHighlighter>
        </Box>
        <Box>
          <Typography variant="h5">CSS</Typography>
          <SyntaxHighlighter language="css" style={monokaiSublime} showLineNumbers>
            {`*[part~=day6], *[part~=day0] {
  color: rgb(194, 19, 19);
  font-weight: bold;
}`}
          </SyntaxHighlighter>
        </Box>
      </Box>
      <Box className={clsx(basicStyle.code, basicStyle['one-col'])}>
        <Typography variant="h5">Výsledek</Typography>
        <Box className={basicStyle.result}>
          <Iframe src="iframes/customElementFixed.html" />
        </Box>
      </Box>
    </Box>
  </>
);

export default ShadowDomPartC;
