import { Box, Typography } from '@mui/material';
import { Helmet } from 'react-helmet';
import SyntaxHighlighter from 'react-syntax-highlighter';
import { monokaiSublime } from 'react-syntax-highlighter/dist/esm/styles/hljs';
import { Reference, ValuesList } from 'Components';
import basicStyle from './BasicStyles.module.css';

const WritingModes = () => (
  <>
    <Helmet>
      <title>writing modes | State of CSS 2022 outsiders</title>
    </Helmet>
    <h1>Writing modes</h1>
    <Reference
      canIUseUrl="https://caniuse.com/css-writing-mode"
      mdnUrl="https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Writing_Modes"
      chrome={48}
      edge={79}
      firefox={50}
      statsName="writing modes"
      opera={35}
      safari={14}
      title="Sada vlastností writing modes"
    >
      <p>Řeší zápis neevropských jazyků (čínština, hebrejština, arabština)</p>
      <ValuesList
        title="Patří sem tyto vlastnosti"
        list={[
          {
            id: 'direction',
            primary: 'direction',
            secondary: (
              <>
                Směr zápisu. Možné hodnoty:
                {' '}
                <code>ltr | rtl</code>
                .
              </>),
          },
          {
            id: 'writing-mode',
            primary: 'writing-mode',
            secondary: (
              <>
                Nastavuje rozložení textu horizontálně nebo vertikálně.
                Možné hodnoty
                {' '}
                <code>horizontal-tb | vertical-rl | vertical-lr</code>
                .
              </>),
          },
          {
            id: 'text-combine-upright',
            primary: 'text-combine-upright',
            secondary: (
              <>
                Nepodporováno v Edge a v Safari.
                Stlačí znaky do šířky jednoho znaku.
                Použití v čínštině a japonštině.
                Možné hodnoty
                {' '}
                <code>none | all</code>
                .
              </>),
          },
          {
            id: 'text-orientation',
            primary: 'text-orientation',
            secondary: (
              <>
                Nastavuje Orientaci znaků v řádku.
                Je k dispozici jen pro vertikální texty.
                Možné hodnoty
                {' '}
                <code>mixed | upright</code>
                .
              </>),
          },
          { id: 'unicode-bidi', primary: 'unicode-bidi', secondary: 'Řídí vykreslení textu v případě, že je směr rtl i ltr.' },
        ]}
      />
    </Reference>
    <Box>
      <Box className={basicStyle.code}>
        <Box>
          <Typography variant="h5">CSS</Typography>
          <SyntaxHighlighter language="css" style={monokaiSublime} showLineNumbers>
            {`/* nějaké ty formátovací styly nevypisuju */

.rtl {
  direction: rtl;
}

.tb {
  writing-mode: horizontal-tb;
}

.lr {
  writing-mode: vertical-lr;
}

.rl {
  writing-mode: vertical-rl;
}

.combine {
  writing-mode: vertical-lr;
}

.combine.none span {
  text-combine-upright: none;
}

.combine.all span {
  text-combine-upright: all;
}

.mixed {
  text-orientation: mixed;
}

.upright {
  text-orientation: upright;
}
`}
          </SyntaxHighlighter>
        </Box>
        <Box>
          <Typography variant="h5">HTML</Typography>
          <SyntaxHighlighter language="xml" style={monokaiSublime} showLineNumbers>
            {`<h6>direction: rtl</h6>

<p class="rtl">
  Tenhle text je psaný v latince;
  je formátován pro čtení zprava
  doleva. Kde jsou ty interpunkce?
</p>

<p class="rtl" lang="ar">
  هذا النص مترجم آليًا
  إلى اللغة العربية.
  لا اعرف كلمة عربية. لكن من يدري ،
  ربما سأتعلمه يومًا ما.
</p>

<h6>writing-mode</h6>
<div>

  <p class="tb">
    horizontal-tb
    Něco sem musím napsat.
  </p>

  <p class="lr">
    vertical-lr
    Něco sem musím napsat.
  </p>

  <p class="rl">
    vertical-rl
    Něco sem musím napsat.
  </p>
</div>

<h6>text-combine-upright</h6>
<div class="box">

  <p class="combine none" lang="ch">
    <span>none</span>
    <span>2023</span>
    年，捷克广播电台成立
    <span>100</span>
    周年（<span>1923</span>
    年<span>5</span>月
    <span>18</span>日）
  </p>

  <p class="combine all" lang="ch">
    <span>all</span>
    <span>2023</span>
    年，捷克广播电台成立
    <span>100</span>
    周年（<span>1923</span>
    年<span>5</span>月
    <span>18</span>日）
  </p>

</div>

<h6>text-orientation</h6>
<div class="box">

  <p class="lr mixed">
    mixed: Zase trochu delší
    text, ale už na víc řádků.
    Kdo nevěří, tak ať nevěří.
  </p>

  <p class="lr upright">
    upright: Zase trochu delší
    text, ale už na víc řádků.
    Kdo nevěří, tak ať nevěří.
  </p>

</div>
`}
          </SyntaxHighlighter>
        </Box>
        <Box>
          <Typography variant="h5">Výsledek</Typography>
          <Box className={basicStyle.result}>
            <style>
              {`
              .box {
                display: flex;
                gap: 1rem;
              }

              .boxed {
                box-sizing: border-box;
                width: 100px;
                padding: 0.25rem;
                aspect-ratio: 1 / 1;
                border: 1px solid black;
              }

              .rectangle {
                max-width: calc(100vw / 4 - 1rem);
                aspect-ratio: 3 / 1.5;
                border: 1px solid black;
                text-align: center;
                padding: 0.5rem 1rem;
                box-sizing: border-box;
                display: flex;
                justify-content: center;
              }



              .para {
                max-width: 20rem;
              }

              .rtl {
                direction: rtl;
              }

              .tb {
                writing-mode: horizontal-tb;
              }

              .lr {
                writing-mode: vertical-lr;
              }

              .rl {
                writing-mode: vertical-rl;
              }

              .combine {
                writing-mode: vertical-lr;
              }

              .combine.none span {
                text-combine-upright: none;
              }

              .combine.all span {
                text-combine-upright: all;
              }

              .mixed {
                text-orientation: mixed;
              }

              .upright {
                text-orientation: upright;
              }
            `}
            </style>
            <h6>direction: rtl</h6>
            <p className="rtl para">
              Tenhle text je psaný v latince;
              je formátován pro čtení zprava
              doleva. Kde jsou ty interpunkce?
            </p>
            <p className="rtl para" lang="ar" title="Tenhle text je strojově přeložený do arabštiny. Já totiž arabsky neumím ani slovo. Ale kdo ví, třeba se to jednou naučím.">
              هذا النص مترج
              آليًا إلى اللغة العربية.
              لا اعرف كلمة عربية. لكن من يدر ،
              ربما سأتعلمه يومًا ما.
            </p>
            <h6>writing-mode</h6>
            <div className="box">
              <p className="tb boxed para">
                horizontal-tb
                Něco sem musím napsat.
              </p>
              <p className="lr boxed para">
                vertical-lr
                Něco sem musím napsat.
              </p>
              <p className="rl boxed para">
                vertical-rl
                Něco sem musím napsat.
              </p>
            </div>
            <h6>text-combine-upright</h6>
            <div className="box">
              <p className="combine none para" lang="ch" title="V roce 2023 uplynulo 100 let od založení Českého rozhlasu (18. května 1923)">
                <span>none</span>
                <span>2023</span>
                年，捷克广播电台成立
                <span>100</span>
                周年（
                <span>1923</span>
                年
                <span>5</span>
                月
                <span>18</span>
                日）
              </p>
              <p className="combine all para" lang="ch" title="V roce 2023 uplynulo 100 let od založení Českého rozhlasu (18. května 1923)">
                <span>all</span>
                <span>2023</span>
                年，捷克广播电台成立
                <span>100</span>
                周年（
                <span>1923</span>
                年
                <span>5</span>
                月
                <span>18</span>
                日）
              </p>
            </div>
            <h6>text-orientation</h6>
            <div>
              <p className="lr mixed rectangle">
                mixed: Zase trochu delší text, ale už na víc řádků.
                Kdo nevěří, tak ať nevěří.
              </p>
              <p className="lr upright rectangle">
                upright: Zase trochu delší text, ale už na víc řádků.
                Kdo nevěří, tak ať nevěří.
              </p>
            </div>
          </Box>
        </Box>
      </Box>
    </Box>
  </>
);

export default WritingModes;
