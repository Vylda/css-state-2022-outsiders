import {
  Box, ButtonGroup, Typography,
} from '@mui/material';
import clsx from 'clsx';
import { useState } from 'react';
import { Helmet } from 'react-helmet';
import SyntaxHighlighter from 'react-syntax-highlighter';
import { monokaiSublime } from 'react-syntax-highlighter/dist/esm/styles/hljs';
import { CodeButton, Reference, ValuesList } from 'Components';
import basicStyle from './BasicStyles.module.css';

const OverscrollBehavior = () => {
  const [elementClass, setElementClass] = useState('auto');

  return (
    <>
      <Helmet>
        <title>overscroll-behavior | State of CSS 2022 outsiders</title>
      </Helmet>
      <h1>overscroll-behavior</h1>
      <Reference
        canIUseUrl="https://caniuse.com/css-overscroll-behavior"
        mdnUrl="https://developer.mozilla.org/en-US/docs/Web/CSS/overscroll-behavior"
        chrome={65}
        edge={79}
        firefox={59}
        statsName="overscroll-behavior"
        opera={52}
        safari={16.0}
        title="Vlastnost overscroll-behavior"
      >
        <p>
          Řeší skrolování ve více skrolovatelných kontejnerech.
        </p>
        <p>
          Je to zkratka za
          {' '}
          <code>overscroll-behavior-x</code>
          {' '}
          a
          {' '}
          <code>overscroll-behavior-y</code>
          .
        </p>
        <p>
          Na mobilu to může vyřešit nechtěné obnovení stránky při potáhnutí dolů (viz
          {' '}
          <a href="https://mdn.github.io/css-examples/overscroll-behavior/" target="_blank" rel="noreferrer">demo na MDN</a>
          ,
          {' '}
          <a href="https://github.com/mdn/css-examples/tree/main/overscroll-behavior" target="_blank" rel="noreferrer">zdroják</a>
          ).
        </p>
        <ValuesList list={[
          {
            id: 'auto',
            primary: 'auto',
            secondary: 'Při doskrolování vnořeného začne skrolovat nadřazený.',
          },
          {
            id: 'contain',
            primary: 'contain',
            secondary: 'Skroluje jen ten element, který má nad sebou ukazatel (myš, prst)',
          },
          {
            id: 'none',
            primary: 'none',
            secondary: (
              <>
                Chová se stejně jako
                {' '}
                <code>contain</code>
                , ale neudělá refresh (když se dá na &lt;html&gt; element).
              </>),
          },
        ]}
        />
      </Reference>
      <Box>
        <Box className={basicStyle.code}>
          <Box>
            <Typography variant="h5">CSS</Typography>
            <SyntaxHighlighter language="css" style={monokaiSublime} showLineNumbers>
              {`/* nějaké ty formátovací styly nevypisuju */

.outer {
    width: 250px;
    height: 400px;
    position: relative;
  }

  .inner {
    width: 150px;
    height: 200px;
    border-color: orange;
    position: absolute;
    top: 0.25rem;
    right: 0.25rem;
    font-size: 0.9rem;
    box-shadow: -3px 3px 3px rgb(0 0 0 / 50%);

    overscroll-behavior: ${elementClass};
  }`}
            </SyntaxHighlighter>
          </Box>
          <Box>
            <Typography variant="h5">HTML</Typography>
            <SyntaxHighlighter language="xml" style={monokaiSublime} showLineNumbers>
              {`<div class="outer">
  Tohle je rodičovský prvek, …

  <div class="inner">
    Tohle je potomek svého rodiče…
  </div>
</div>`}
            </SyntaxHighlighter>
          </Box>
          <Box>
            <Typography variant="h5">Výsledek</Typography>
            <Box className={basicStyle.result}>
              <style>
                {`
            .scroll {
              overflow-y: scroll;
              box-sizing: border-box;
              border: 1px solid green;
              background-color: rgb(255 255 255 / 0.9);
              padding: 0.5rem;
              font-size: 0.9rem;
            }

            .outer {
              width: 250px;
              height: 400px;
              position: relative;
            }

            .inner {
              width: 150px;
              height: 200px;
              border-color: orange;
              position: absolute;
              top: 0.25rem;
              right: 0.25rem;
              font-size: 0.9rem;
              box-shadow: -3px 3px 3px rgb(0 0 0 / 50%);

              overscroll-behavior:  ${elementClass};
            }
            `}
              </style>
              <ButtonGroup variant="contained" color="primary" size="small" sx={{ marginBottom: '0.5rem' }}>
                <CodeButton
                  showStartIcon={elementClass === 'auto'}
                  onClick={() => setElementClass('auto')}
                >
                  auto
                </CodeButton>
                <CodeButton
                  showStartIcon={elementClass === 'contain'}
                  onClick={() => setElementClass('contain')}
                >
                  contain
                </CodeButton>
                <CodeButton
                  showStartIcon={elementClass === 'none'}
                  onClick={() => setElementClass('none')}
                >
                  none
                </CodeButton>

              </ButtonGroup>
              <div className="scroll outer">
                Tohle je rodičovský prvek, ve kterém je vložen další skrolovací div.
                Je tady strašně moc textu, který se při dobré vůli bude skrolovat.
                Ono to sice nevypadá, ale když jsem se dnes ráno probudil,
                tak jsem se podíval z okna a uviděl jsem tam to, co běžně vidím
                na fotkách kamarádů na Fejsu: trávu, stromy a nebe.
                Tu zpíval ptáček jarabáček, tu sysel vynořil čumáček z nory.
                Mraky na nebi tvořily rozpustilé tvary a vítr v listoví okolních stromů
                zpíval ranní písničku k potěše všech živých bytostí co jich tu jen bylo.
                No a pak přiletěli Vogoni a celou Zemi srovnali se vzduchopráznem.
                <div className={clsx('scroll', 'inner')}>
                  Tohle je potomek svého rodiče a taky se v něm dá skrolovat.
                  Ale můžeme vyzkoušet, zdali bude skrolovat i ten rodič.
                  Kdo se kouká pořádně, tak ví, že něco na tom bude.
                  Ale pozor, může se to zvrtnout, takže se to celé převrátí.
                  V ten okamžik se všehomír zastaví.
                </div>
              </div>
            </Box>
          </Box>
        </Box>
      </Box>
    </>
  );
};

export default OverscrollBehavior;
