import { useRouteError } from 'react-router-dom';
import style from './ErrorPage.module.css';

const ErrorPage = () => {
  const { data, status, statusText } = useRouteError();
  console.error(`${data} (${status}: ${statusText})`);

  return (
    <main className={style.main}>
      <div className={style.box}>
        <h1>Sorry, no.</h1>
        { status === 404
          ? (
            <>
              <svg
                className={style.magnifier}
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 -20 480 480"
              >
                <defs>
                  <linearGradient
                    id="fountain_fill_1"
                    x1="-184.78"
                    y1="345.99"
                    x2="-303.69"
                    y2="425.21"
                    gradientTransform="translate(1793.63 2611.94) scale(5.85 -5.85)"
                    gradientUnits="userSpaceOnUse"
                  >
                    <stop offset="0" stopColor="#000" />
                    <stop offset="1" stopColor="#000" stopOpacity="0" />
                  </linearGradient>
                  <radialGradient
                    id="fountain_fill_2"
                    cx="-558.78"
                    cy="605.3"
                    fx="-550.36"
                    fy="589.78"
                    r="27.96"
                    gradientTransform="translate(4230.6 3503.24) rotate(-3.2) scale(6.89 -5.91) skewX(-.29)"
                    gradientUnits="userSpaceOnUse"
                  >
                    <stop offset="0" stopColor="#fff" stopOpacity=".57" />
                    <stop offset="1" stopColor="#96b0c6" stopOpacity=".9" />
                  </radialGradient>
                  <linearGradient
                    id="fountain_fill_3"
                    x1="-563.04"
                    y1="625.58"
                    x2="-560.22"
                    y2="598.36"
                    gradientTransform="translate(4028.83 3288.33) rotate(-1.7) scale(6.72 -5.46) skewX(-.15)"
                    gradientUnits="userSpaceOnUse"
                  >
                    <stop offset="0" stopColor="#fff" stopOpacity=".75" />
                    <stop offset="1" stopColor="#fff" stopOpacity="0" />
                  </linearGradient>
                  <linearGradient
                    id="fountain_fill_4"
                    x1="-578.72"
                    y1="594.15"
                    x2="-566.89"
                    y2="529.78"
                    gradientTransform="translate(4250.02 2537.18) rotate(-1.7) scale(6.95 -4.49) skewX(-.12)"
                    gradientUnits="userSpaceOnUse"
                  >
                    <stop offset="0" stopColor="#fff" stopOpacity=".75" />
                    <stop offset="1" stopColor="#fff" stopOpacity="0" />
                  </linearGradient>
                  <linearGradient
                    id="fountain_fill_5"
                    x1="-495.9"
                    y1="576.53"
                    x2="-502.26"
                    y2="569.43"
                    gradientTransform="translate(3460.24 3512.91) rotate(-1.79) scale(5.96 -5.74)"
                    gradientUnits="userSpaceOnUse"
                  >
                    <stop offset="0" stopColor="#fff" stopOpacity=".75" />
                    <stop offset="1" stopColor="#fff" stopOpacity="0" />
                  </linearGradient>
                  <linearGradient
                    id="fountain_fill_6"
                    x1="-250.25"
                    y1="381.94"
                    x2="-242.12"
                    y2="390.5"
                    gradientTransform="translate(1793.63 2611.94) scale(5.85 -5.85)"
                    gradientUnits="userSpaceOnUse"
                  >
                    <stop offset="0" stopColor="#000" />
                    <stop offset="1" stopColor="#000" stopOpacity="0" />
                  </linearGradient>
                </defs>
                <path
                  d="m134.68,67.13c-2.56.28-5.05.56-7.62.98C44.66,81.4-11.37,157.66,1.96,238.37s90.94,135.58,173.34,122.29c31.13-5.02,58.46-18.98,79.74-38.85l173.63,125.93c18.25-2.54,31.14-14.89,38.81-37.21l-182.91-125-3.52,4.13c17.54-28.94,25.23-63.73,19.33-99.45-12.91-78.19-86.23-131.88-165.7-123.08h0Zm.68,6.18c76.12-8.43,146.51,43.06,158.87,117.95,12.76,77.31-48.58,150.52-127.49,163.25S21.07,314.8,8.3,237.5,49.18,86.99,128.1,74.26c2.47-.4,4.8-.68,7.26-.95h0Z"
                  fill="url(#fountain_fill_1)"
                  fillRule="evenodd"
                />
                <path
                  d="m298.99,234.07l173.18,158.16c-11.07,22.66-26.32,33.97-45.67,34.14l-163.84-158.45,36.33-33.85h0Z"
                  fill="#807d74"
                  fillRule="evenodd"
                />
                <path
                  d="m303.79,222.44C154.22,378.53-37.45,199.48,63.81,55.55c-131.41,156.17,99.71,346.4,239.98,166.89h0Z"
                  fill="rgba(39, 63, 87, 1)"
                />
                <g>
                  <path
                    d="m115.94,180.65l11.26,4.14-13.73,33.23h28.96v9.92h-42.6v-8.79l16.11-38.5Zm20.67,23.39v36.07h-12.68l-.17-20.76,1.67-15.32h11.17Z"
                  />
                  <path
                    d="m171.43,180.65c6.92,0,12.14,2.67,15.67,8.01,3.53,5.34,5.29,12.78,5.29,22.33s-1.77,17.05-5.29,22.45c-3.53,5.4-8.75,8.1-15.67,8.1s-12.21-2.7-15.71-8.1-5.25-12.88-5.25-22.45,1.75-16.98,5.25-22.33c3.5-5.34,8.74-8.01,15.71-8.01Zm6.57,11.42l3.39,7.24-15.48,31.97-3.81-5.94,15.9-33.27Zm-6.57-1.84c-2.96,0-5.12,1.58-6.49,4.75-1.37,3.17-2.05,8.5-2.05,16.01,0,4.35.13,7.9.4,10.65.26,2.75.72,4.88,1.36,6.38.64,1.51,1.51,2.55,2.62,3.12,1.1.57,2.49.86,4.16.86,1.98,0,3.59-.67,4.83-2.01,1.24-1.34,2.16-3.55,2.74-6.63.59-3.08.88-7.2.88-12.37,0-5.69-.25-10.03-.75-13.02-.5-2.99-1.36-5.02-2.57-6.11-1.21-1.09-2.92-1.63-5.13-1.63Z"
                  />
                  <path
                    d="m216.38,180.65l11.26,4.14-13.73,33.23h28.96v9.92h-42.6v-8.79l16.11-38.5Zm20.67,23.39v36.07h-12.68l-.17-20.76,1.67-15.32h11.17Z"
                  />
                </g>
                <path
                  d="m328.99,139.52c2.55,77.57-61.61,142.48-143.2,144.9S35.9,225.86,33.36,148.29,94.97,5.81,176.56,3.39s149.89,58.56,152.43,136.13Z"
                  fill="url(#fountain_fill_2)"
                  fillRule="evenodd"
                  stroke="rgba(49, 78, 108, 1)"
                  strokeMiterlimit="23.39"
                  strokeWidth="6.64"
                />
                <path
                  d="m242.92,41.93c20.88,25.04-1.11,74.71-49.09,110.87s-103.87,45.18-124.75,20.14,1.11-74.71,49.09-110.87,103.87-45.18,124.75-20.14h0Z"
                  fill="url(#fountain_fill_3)"
                  fillOpacity=".09"
                  fillRule="evenodd"
                />
                <path
                  d="m99.83,244.54c84.75,25.2,186.27-26.12,212.99-106.52,25.81,73.53-136.13,231.16-212.99,106.52Z"
                  fill="url(#fountain_fill_4)"
                  fillOpacity=".55"
                  fillRule="evenodd"
                />
                <path
                  d="m272.21,267.17l27.03-25.63,163.56,150.24c7.21,19.46-2.61,33.22-35.67,28.54l-154.92-153.14h0Z"
                  fill="url(#fountain_fill_5)"
                  fillOpacity=".75"
                  fillRule="evenodd"
                />
                <path
                  d="m268.41,270.85l27.03-25.63,163.56,150.24c7.21,19.46-2.61,33.22-35.67,28.54l-154.92-153.14h0Z"
                  fill="url(#fountain_fill_6)"
                  fillRule="evenodd"
                />
              </svg>
              <p>Ať, dělám, co dělám, prostě to nemůžu najít.</p>
            </>
          )
          : <p>Stalo se něco zákeřného!</p>}
        <p>
          Nechceš to vzít raději
          {' '}
          <a className={style.anchor} href="./" title="začátek">od začátku</a>
          ?
        </p>
      </div>
    </main>
  );
};

export default ErrorPage;
