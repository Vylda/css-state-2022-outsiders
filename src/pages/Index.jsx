import { Paper } from '@mui/material';
import { Helmet } from 'react-helmet';
import style from './Index.module.css';

const Index = () => (
  <>
    <Helmet>
      <title>State of CSS 2022 outsiders</title>
    </Helmet>
    <div className={style.main}>
      <Paper elevation={3} className={style.paper}>
        <h1 className={style.header}>State of CSS 2022 outsiders</h1>
        <h2 className={style.subheader}>Vylda</h2>
      </Paper>
    </div>
  </>
);

export default Index;
