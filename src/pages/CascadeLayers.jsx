import VisibilityIcon from '@mui/icons-material/Visibility';
import VisibilityOffIcon from '@mui/icons-material/VisibilityOff';
import {
  Box, Button, ButtonGroup, Typography,
} from '@mui/material';
import clsx from 'clsx';
import { useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import SyntaxHighlighter from 'react-syntax-highlighter';
import { monokaiSublime } from 'react-syntax-highlighter/dist/esm/styles/hljs';
import { Reference } from 'Components';
import basicStyle from './BasicStyles.module.css';

const CascadeLayers = () => {
  const DEFAULT_TEXT = 'Tak jak to bude?';
  const [showCode, setShowCode] = useState(false);
  const [text, setText] = useState(DEFAULT_TEXT);

  useEffect(() => {
    if (showCode) {
      setInterval(() => {
        setText('Ó, můj bóže, já jsem růžovej!');
      }, 5000);
      return;
    }

    setText(DEFAULT_TEXT);
  }, [showCode]);

  return (
    <>
      <Helmet>
        <title>Cascade layers | State of CSS 2022 outsiders</title>
      </Helmet>
      <h1>Cascade layers (@layer) </h1>
      <Reference
        canIUseUrl="https://caniuse.com/css-cascade-layers"
        mdnUrl="https://developer.mozilla.org/en-US/docs/Web/CSS/@layer"
        chrome={99}
        edge={99}
        firefox={97}
        statsName="cascade layers"
        opera={86}
        safari={15.4}
        title="Pravidlo @layer"
      >
        <p>
          Super věc, když chcete přepsat vlastnosti prvků, na které nemůžete sáhnout.
          Krásným příkladem jsou cizí komponenty (nebo importované CSS).
        </p>
        <p>
          Ovšem na
          {' '}
          <code>!important</code>
          {' '}
          to nemá vliv.
        </p>
      </Reference>
      <Box>
        <Box className={basicStyle.code}>
          <Box>
            <Typography variant="h5">CSS</Typography>
            <SyntaxHighlighter language="css" style={monokaiSublime} showLineNumbers>
              {`@layer reset, defaults, custom;

@layer reset {
  #para.test {
    font-size: 20px;
    background-color: darkgreen;
    color: white;
    font-weight: 800;
    padding: 1rem;
    border-radius: 0.25rem;
  }
}

@layer custom {
  .test {
    background-color: pink;
    color: black;
  }
}

@layer defaults {
  .test {
    background-color: orange;
    color: darkblue;
    font-weight: 300;
  }
}
`}
            </SyntaxHighlighter>
          </Box>
          <Box>
            <Typography variant="h5">HTML</Typography>
            <SyntaxHighlighter language="xml" style={monokaiSublime} showLineNumbers>
              {'<p class="test" id="para">Tak jak to bude?</p>'}
            </SyntaxHighlighter>
          </Box>
          <Box>
            <Typography variant="h5">Výsledek</Typography>
            <Box className={basicStyle.result}>
              <ButtonGroup variant="contained" color="primary" size="small" sx={{ marginBottom: '1rem' }}>
                <Button
                  startIcon={showCode ? <VisibilityOffIcon /> : <VisibilityIcon />}
                  onClick={() => setShowCode(!showCode)}
                >
                  {`${showCode ? 'Skrýt' : 'Ukázat'} výsledek`}
                </Button>
              </ButtonGroup>
              <style>
                {`
                @layer reset, defaults, custom;
                .hide {
                  display: none;
                }

                @layer reset {
                  #para.test {
                    font-size: 20px;
                    background-color: darkgreen;
                    color: white;
                    font-weight: 800;
                    padding: 1rem;
                    border-radius: 0.25rem;
                  }
                }

                @layer custom {
                  .test {
                    background-color: pink;
                    color: black;
                  }
                }

                @layer defaults {
                  .test {
                    background-color: orange;
                    color: darkblue;
                    font-weight: 300;
                  }
                }
                `}
              </style>
              <p className={clsx('test', !showCode && 'hide')} id="para">{text}</p>
            </Box>
          </Box>
        </Box>
      </Box>
    </>
  );
};

export default CascadeLayers;
