import { Box, Typography } from '@mui/material';
import clsx from 'clsx';
import { Helmet } from 'react-helmet';
import SyntaxHighlighter from 'react-syntax-highlighter';
import { monokaiSublime } from 'react-syntax-highlighter/dist/esm/styles/hljs';
import { Reference } from 'Components';
import basicStyle from './BasicStyles.module.css';

const ImageSet = () => (
  <>
    <Helmet>
      <title> image-set() | State of CSS 2022 outsiders</title>
    </Helmet>
    <h1>image-set()</h1>
    <Reference
      canIUseUrl="https://caniuse.com/css-image-set"
      mdnUrl="https://developer.mozilla.org/en-US/docs/Web/CSS/image/image-set"
      chrome={{ version: 21, partial: true }}
      edge={{ version: 79, partial: true }}
      firefox={{ version: 89, partial: true }}
      statsName="image-set()"
      opera={{ version: 15, partial: true }}
      safari={{ version: 6, partial: true }}
      title="Vlastnost image-set() pro pozadí"
    >
      <p>
        Umožňuje mít různá pozadí pro různá rozlišení: normální a vysoké.
      </p>
      <p>
        Chrome, Edge a Opera umí obrázky jen ve funkci
        {' '}
        <code>url()</code>
        {' '}
        (takže ne gradienty) a rozlišení
        {' '}
        <code>x</code>
        .
        Musí mít vendor prefix
        {' '}
        <code>-webkit-</code>
        .
      </p>
      <p>
        Firefox neumí generované obrázky (gradienty).
      </p>
      <p>
        Safari neumí specifikovat typ (třeba
        {' '}
        <code>type(&quot;image/avif&quot;)</code>
        ).
      </p>
    </Reference>
    <Box>
      <Box className={clsx(basicStyle.code, basicStyle['two-cols'])}>
        <Box>
          <Typography variant="h5">CSS a HTML</Typography>
          <SyntaxHighlighter language="css" style={monokaiSublime} showLineNumbers>
            {`.box {
  width: 150px;
  height: 150px;
  border: 1px solid gray;
}

.s-pozadim {
  background-image: -webkit-image-set(
    url("http://satyr.dev/80/azure?text=1x") 1x,
    url("http://satyr.dev/80/lightcoral?text=2x") 2x);
  background-image: image-set(
    url("http://satyr.dev/80/azure?text=1x") 1x,
    url("http://satyr.dev/80/lightcoral?text=2x") 2x);
}`}
          </SyntaxHighlighter>
          <SyntaxHighlighter language="xml" style={monokaiSublime} showLineNumbers>
            {'<div class="box s-pozadim" />'}
          </SyntaxHighlighter>
        </Box>
        <Box>
          <Typography variant="h5">Výsledek</Typography>
          <Box className={basicStyle.result}>
            <style>
              {`.box {
                width: 150px;
                height: 150px;
                border: 1px solid gray;
              }

              .s-pozadim {
                background-image: -webkit-image-set(
                  url("http://satyr.dev/80/azure?text=1x") 1x,
                  url("http://satyr.dev/80/lightcoral?text=2x") 2x);
                background-image: image-set(
                  url("http://satyr.dev/80/azure?text=1x") 1x,
                  url("http://satyr.dev/80/lightcoral?text=2x") 2x);
              }`}
            </style>
            <div className="box s-pozadim" />
          </Box>
        </Box>
      </Box>
    </Box>
  </>
);

export default ImageSet;
