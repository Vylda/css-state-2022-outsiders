import { Helmet } from 'react-helmet';
import { Reference, ValuesList } from 'Components';

const FontDisplay = () => (
  <>
    <Helmet>
      <title>font-display | State of CSS 2022 outsiders</title>
    </Helmet>
    <h1>font-display</h1>
    <Reference
      canIUseUrl="https://caniuse.com/css-font-rendering-controls"
      mdnUrl="https://developer.mozilla.org/en-US/docs/Web/CSS/@font-face/font-display"
      chrome={60}
      edge={79}
      firefox={58}
      statsName="font-display"
      opera={47}
      safari={11.1}
      title="Vlastnost font-display"
    >
      <p>
        Řídí zobrazení web fontu během stahování.
      </p>
      <ValuesList list={[
        {
          id: 'auto',
          primary: 'auto',
          secondary: 'Zobrazení fontu řídí browser',
        },
        {
          id: 'block',
          primary: 'block',
          secondary: 'Krátce blokuje a pak nekonečně dlouho swapuje.',
        },
        {
          id: 'swap',
          primary: 'swap',
          secondary: 'Extrémně krátký interval blokování a pak nekonečně dlouhé swapování.',
        },
        {
          id: 'fallback',
          primary: 'fallback',
          secondary: 'Extrémně krátký  interval blokování a pak krátký interval swapování.',
        },
        {
          id: 'optional',
          primary: 'optional',
          secondary: 'Extrémně krátký  interval blokování a žádné swapování.',
        },
      ]}
      />
      <p>
        Dobře to popsal
        {' '}
        <a href="https://cz.linkedin.com/in/martinmichalek" title=" Martin Michálek, Web Performance Consultant, Blogger, Head of Frontendisti.cz">Máchal</a>
        {' '}
        na svém webu
        {' '}
        <a href="https://www.vzhurudolu.cz/prirucka/css-font-display" title="Deskriptor font-display ne Vzhůru dolů">Vzhůru dolů</a>
        .
      </p>
    </Reference>
  </>
);

export default FontDisplay;
