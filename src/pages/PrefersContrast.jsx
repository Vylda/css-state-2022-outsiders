import { Box, Typography } from '@mui/material';
import clsx from 'clsx';
import { Helmet } from 'react-helmet';
import SyntaxHighlighter from 'react-syntax-highlighter';
import { monokaiSublime } from 'react-syntax-highlighter/dist/esm/styles/hljs';
import { Reference, ValuesList } from 'Components';
import basicStyle from './BasicStyles.module.css';

const PrefersContrast = () => (
  <>
    <Helmet>
      <title>prefers-contrast | State of CSS 2022 outsiders</title>
    </Helmet>
    <h1>prefers-contrast</h1>
    <Reference
      canIUseUrl="https://caniuse.com/?search=prefers-contrast"
      mdnUrl="https://developer.mozilla.org/en-US/docs/Web/CSS/@media/prefers-contrast"
      chrome={96}
      edge={96}
      firefox={101}
      statsName="prefers-contrast"
      opera={82}
      safari={14.1}
      title="Vlastnost prefers-contrast"
    >
      <p>
        Využívá se k detekci, zda uživatel chce zobrazit stránku s nižším nebo vyšším kontrastem
      </p>

      <ValuesList list={[
        {
          id: 'no-preference',
          primary: 'no-preference',
          secondary: 'Systém nemá nastavenu preferenci kontrastu.',
        },
        {
          id: 'more',
          primary: 'more',
          secondary: 'Vyšší úroveň kontrastu systému.',
        },
        {
          id: 'less',
          primary: 'less',
          secondary: 'Nižší úroveň kontrastu systému.',
        },
        {
          id: 'custom',
          primary: 'custom',
          secondary: (
            <>
              Barevná paleta je specifikována pomocí pravidla
              <code>forced-colors: active</code>
              .
            </>
          ),
        },
      ]}
      />
    </Reference>
    <Box>
      <Box className={clsx(basicStyle.code)}>
        <Box>
          <Typography variant="h5">CSS</Typography>
          <SyntaxHighlighter language="css" style={monokaiSublime} showLineNumbers>
            {`.contrast {
  width: 8rem;
  height: 8rem;
  outline: 2px dashed black;
  margin: 0 1rem;
  display: flex;
  align-items: center;
  text-align: center;
}

@media (prefers-contrast: more) {
  .contrast {
    outline: 2px solid black;
  }
}
`}
          </SyntaxHighlighter>
        </Box>
        <Box>
          <Typography variant="h5">HTML</Typography>
          <SyntaxHighlighter language="xml" style={monokaiSublime} showLineNumbers>
            {`<div class="contrast">
  <span>
    Obrys se mění podle
    požadovaného kontrastu
  </span>
</div>`}
          </SyntaxHighlighter>
        </Box>
        <Box>
          <Typography variant="h5">Výsledek</Typography>
          <Box className={basicStyle.result}>
            <style>
              {`.contrast {
  width: 8rem;
  height: 8rem;
  outline: 2px dashed black;
  margin: 0 1rem;
  display: flex;
  align-items: center;
  text-align: center;
}

@media (prefers-contrast: more) {
  .contrast {
    outline: 2px solid black;
  }
}
`}
            </style>
            <div className="contrast">
              <span>Obrys se mění podle požadovaného kontrastu</span>
            </div>
          </Box>
        </Box>
      </Box>
    </Box>
  </>
);

export default PrefersContrast;
