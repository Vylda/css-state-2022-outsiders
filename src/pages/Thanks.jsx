import { Box } from '@mui/material';
import { Helmet } from 'react-helmet';
import style from './Thanks.module.css';

const Thanks = () => (
  <>
    <Helmet>
      <title>Konec | State of CSS 2022 outsiders</title>
    </Helmet>
    <h1>Tak a to je konec, přátelé…</h1>
    <h2>Díky</h2>

    <Box>
      <Box className={style['qr-wrapper']}>
        <Box className={style['image-container']}>
          <img src="webovka.svg" alt="QR kód na webovku" className={style.image} />
        </Box>
        <Box className={style['image-container']}>
          <img src="gitlab.svg" alt="QR kód na zdroják" className={style.image} />
        </Box>
        <Box className={style['image-container']}>
          <img src="state.svg" alt="QR kód na výsledky průzkumu The State of CSS 2022" className={style.image} />
        </Box>
      </Box>
    </Box>
  </>
);

export default Thanks;
