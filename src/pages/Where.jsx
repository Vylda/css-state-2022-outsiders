import {
  Box, ButtonGroup, Typography,
} from '@mui/material';
import { useState } from 'react';
import { Helmet } from 'react-helmet';
import SyntaxHighlighter from 'react-syntax-highlighter';
import { monokaiSublime } from 'react-syntax-highlighter/dist/esm/styles/hljs';
import { Reference, CodeButton } from 'Components';
import basicStyle from './BasicStyles.module.css';
import style from './Where.module.css';

const Examples = {
  chain: 0,
  specifity: 1,
  badCode: 2,
};
const CodeVariants = {
  old: 0,
  where: 1,
};

const cssCodes = [
  [
    `.prvni .highlight,
.prvni .word,
.druhy .highlight,
.druhy .word,
.ctvrty .highlight,
.ctvrty .word {
  font-weight: bold;
  background-color: rgb(255 255 150);
  display: inline-block;
  padding: 0.25rem;
}`,
    `:where(.prvni, .druhy, .ctvrty)
  :where(.highlight, .word) {
    font-weight: bold;
    background-color: rgb(255 150 230);
    display: inline-block;
    padding: 0.25rem;
}`,
  ],
  [
    `#blue-bold-text {
  font-weight: 800;
  color: rgb(0 0 200);
}

.red-thin-text {
  font-weight: 300;
  color: rgb(200 0 0);
}`,
    `:where(#blue-bold-text){
  font-weight: 800;
  color: rgb(0 0 200);
}

.red-thin-text {
  font-weight: 300;
  color: rgb(200 0 0);
}`,
  ],
  [
    `.green-bold-text,
.green:hokusFokus() {
  font-weight: 800;
  color: rgb(10 180 20);
}`,
    `:where(
  .green-bold-text,
  .green:hokusFokus()
){
  font-weight: 800;
  color: rgb(10 180 20);
}`,
  ],
];

const htmlCodes = [
  `<div class="prvni">
  <span class="highlight">Tohle</span> je text
  <span class="word">prvního</span> odstavce.
</div>

<div class="druhy">
  <span class="highlight">Tohle</span> je text
  <span class="word">druhého</span> odstavce.
</div>

<div class="treti">
  <span class="highlight">Tohle</span> je text
  <span class="word">třetího</span> odstavce.
</div>

<div class="ctvrty">
  <span class="highlight">Tohle</span> je text
  <span class="word">čtvrtého</span> odstavce.
</div>`,
  `<div id="blue-bold-text">
  Hodně hustej tučnej modrej text.
</div>

<div id="blue-bold-text" class="red-thin-text">
  Hodně hustej netučnej červenej text.
</div>`,
  `<div class="green-bold-text">
  Hodně hustej tučnej zelenej text.
</div>`,
];

const Where = () => {
  const [example, setExample] = useState(Examples.chain);
  const [codeVariant, setCodeVariant] = useState(CodeVariants.old);

  return (
    <>
      <Helmet>
        <title>:where() | State of CSS 2022 outsiders</title>
      </Helmet>
      <h1>:where()</h1>
      <Reference
        canIUseUrl="https://caniuse.com/mdn-css_selectors_where"
        mdnUrl="https://developer.mozilla.org/en-US/docs/Web/CSS/:where"
        chrome={88}
        edge={88}
        firefox={78}
        statsName=":where()"
        opera={74}
        safari={14}
        title="Pseudotřída :where()"
      >
        <p>
          Vybere prvky podle seznamu selektorů. Na rozdíl od selektoru (pseudotřídy)
          {' '}
          <code>:is()</code>
          {' '}
          má nulovou specificitu.
        </p>
        <p>
          Úplně geniální věc hlavně při řetězení poměrně
          kombinovaných selektorů nebo při práci se specificitou.
        </p>
        <p>
          Taky to ignoruje chybně zapsané CSS selektory, které by shodily celé pravidlo.
        </p>
      </Reference>
      <Box>
        <Box className={basicStyle.code}>
          <Box>
            <Typography variant="h5">CSS</Typography>
            <SyntaxHighlighter language="css" style={monokaiSublime} showLineNumbers>
              {cssCodes[example][codeVariant]}
            </SyntaxHighlighter>
          </Box>
          <Box>
            <Typography variant="h5">HTML</Typography>
            <SyntaxHighlighter language="xml" style={monokaiSublime} showLineNumbers>
              {htmlCodes[example]}
            </SyntaxHighlighter>
          </Box>
          <Box>
            <Typography variant="h5">Výsledek</Typography>
            <Box className={basicStyle.result}>
              <div className={style['button-box']}>
                <ButtonGroup variant="contained" color="primary" size="small" sx={{ marginBottom: '0.5rem' }}>
                  <CodeButton
                    showStartIcon={example === Examples.chain}
                    onClick={() => {
                      setExample(Examples.chain);
                      setCodeVariant(CodeVariants.old);
                    }}
                  >
                    Řetězení
                  </CodeButton>
                  <CodeButton
                    showStartIcon={example === Examples.specifity}
                    onClick={() => {
                      setExample(Examples.specifity);
                      setCodeVariant(CodeVariants.old);
                    }}
                  >
                    Specificita
                  </CodeButton>
                  <CodeButton
                    showStartIcon={example === Examples.badCode}
                    onClick={() => {
                      setExample(Examples.badCode);
                      setCodeVariant(CodeVariants.old);
                    }}
                  >
                    Špatné CSS
                  </CodeButton>

                </ButtonGroup>
                <ButtonGroup variant="contained" color="primary" size="small" sx={{ marginBottom: '0.5rem' }}>
                  <CodeButton
                    showStartIcon={codeVariant === CodeVariants.old}
                    onClick={() => {
                      setCodeVariant(CodeVariants.old);
                    }}
                  >
                    Postaru
                  </CodeButton>
                  <CodeButton
                    showStartIcon={codeVariant === CodeVariants.where}
                    onClick={() => {
                      setCodeVariant(CodeVariants.where);
                    }}
                  >
                    :where()
                  </CodeButton>
                </ButtonGroup>
              </div>
              <style>{cssCodes[example][codeVariant]}</style>
              {example === Examples.chain && (
              <>
                <div className="prvni">
                  <span className="highlight">Tohle</span>
                  {' '}
                  je text
                  {' '}
                  <span className="word">prvního</span>
                  {' '}
                  odstavce.
                </div>
                <div className="druhy">
                  <span className="highlight">Tohle</span>
                  {' '}
                  je text
                  {' '}
                  <span className="word">druhého</span>
                  {' '}
                  odstavce.
                </div>
                <div className="treti">
                  <span className="highlight">Tohle</span>
                  {' '}
                  je text
                  {' '}
                  <span className="word">třetího</span>
                  {' '}
                  odstavce.
                </div>
                <div className="ctvrty">
                  <span className="highlight">Tohle</span>
                  {' '}
                  je text
                  {' '}
                  <span className="word">čtvrtého</span>
                  {' '}
                  odstavce.
                </div>
              </>
              )}
              {example === Examples.specifity && (
              <>
                <div id="blue-bold-text">
                  Hodně hustej tučnej modrej text.
                </div>
                <div id="blue-bold-text" className="red-thin-text">
                  Hodně hustej netučnej červenej text.
                </div>
              </>
              )}
              {example === Examples.badCode && (
                <div className="green-bold-text">
                  Hodně hustej tučnej zelenej text.
                </div>
              )}
            </Box>
          </Box>
        </Box>
      </Box>
    </>
  );
};
export default Where;
