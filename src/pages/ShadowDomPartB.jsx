import { Box, Typography } from '@mui/material';
import clsx from 'clsx';
import { Helmet } from 'react-helmet';
import SyntaxHighlighter from 'react-syntax-highlighter';
import { monokaiSublime } from 'react-syntax-highlighter/dist/esm/styles/hljs';
import { Iframe, Reference } from 'Components';
import basicStyle from './BasicStyles.module.css';

const ShadowDomPartB = () => (
  <>
    <Helmet>
      <title>Shadow DOM ::part() - použití elementu | State of CSS 2022 outsiders</title>
    </Helmet>
    <h1>Shadow DOM ::part() - použití elementu</h1>
    <Reference
      canIUseUrl="https://caniuse.com/mdn-api_element_part"
      mdnUrl="https://developer.mozilla.org/en-US/docs/Web/API/Element/part"
      chrome={67}
      edge={79}
      firefox={63}
      opera={64}
      safari={{ version: 10.1, partial: true }}
      title="Custom element"
    >
      <p>
        Custom elementy jsou znovupoužitelné elementy, založené na Shadow DOM, takže jejich
        vzhled a vlastnosti jsou odstíněné od běžného DOMu.
      </p>
      <p>
        Hlavní styly a JavaScript tak nemůžou ovlivnit Custom element.
      </p>
    </Reference>
    <Box>
      <Box className={clsx(basicStyle.code)}>
        <Box>
          <Typography variant="h5">HTML</Typography>
          <SyntaxHighlighter language="xml" style={monokaiSublime} showLineNumbers>
            {`<p>
  <small-callendar />
</p>`}
          </SyntaxHighlighter>
        </Box>
        <Box>
          <Typography variant="h5">JavaScript</Typography>
          <SyntaxHighlighter language="javascript" style={monokaiSublime} showLineNumbers>
            {`globalThis
  .customElements.define(
    'small-callendar', SmallCallendar,
  );`}
          </SyntaxHighlighter>
        </Box>
        <Box>
          <Typography variant="h5">CSS</Typography>
          <SyntaxHighlighter language="css" style={monokaiSublime} showLineNumbers>
            {`*[part~=day6], *[part~=day0] {
  color: rgb(194, 19, 19);
  font-weight: bold;
}`}
          </SyntaxHighlighter>
        </Box>
      </Box>
      <Box className={clsx(basicStyle.code, basicStyle['one-col'])}>
        <Typography variant="h5">Výsledek</Typography>
        <Box className={basicStyle.result}>
          <Iframe src="iframes/customElement.html" />
        </Box>
      </Box>
    </Box>
  </>
);

export default ShadowDomPartB;
