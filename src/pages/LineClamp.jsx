import ZoomInMapIcon from '@mui/icons-material/ZoomInMap';
import ZoomOutMapIcon from '@mui/icons-material/ZoomOutMap';
import {
  Box, Button, ButtonGroup, Typography,
} from '@mui/material';
import { useState } from 'react';
import { Helmet } from 'react-helmet';
import SyntaxHighlighter from 'react-syntax-highlighter';
import { monokaiSublime } from 'react-syntax-highlighter/dist/esm/styles/hljs';
import { Reference } from 'Components';
import basicStyle from './BasicStyles.module.css';

const LineClamp = () => {
  const [long, setLong] = useState(false);

  return (
    <>
      <Helmet>
        <title>line-clamp | State of CSS 2022 outsiders</title>
      </Helmet>
      <h1>line-clamp</h1>
      <Reference
        canIUseUrl="https://caniuse.com/css-line-clamp"
        mdnUrl="https://developer.mozilla.org/en-US/docs/Web/CSS/-webkit-line-clamp"
        chrome={14}
        edge={17}
        firefox={68}
        statsName="line-clamp"
        opera={15}
        safari={5}
        title="Vlastnost line-clamp"
      >
        <p>
          Jupí! Konečně podpora tří teček na více řádcích! Bohužel s vendor prefixem
          {' '}
          <code>-webkit-</code>
          .
        </p>
        <p>
          Prozatím je tam nutné mít vlastnosti
          {' '}
          <code>display: -webkit-box;</code>
          {' '}
          a
          {' '}
          <code>-webkit-box-orient: vertical;</code>
          . Tak uvidíme.
        </p>
      </Reference>
      <Box>
        <Box className={basicStyle.code}>
          <Box>
            <Typography variant="h5">CSS</Typography>
            <SyntaxHighlighter language="css" style={monokaiSublime} showLineNumbers>
              {`
br {
  display: block;
  margin: 0.25rem 0;
}

.box {
  width: ${long ? '90' : '50'}%;
  box-shadow: 0 0 5px 2px rgb(0 0 0 / 0.5);

  overflow: hidden;

  display: -webkit-box;
  -webkit-box-orient: vertical;

  -webkit-line-clamp: 5;
  line-clamp: 5;
}
`}
            </SyntaxHighlighter>
          </Box>
          <Box>
            <Typography variant="h5">HTML</Typography>
            <SyntaxHighlighter language="xml" style={monokaiSublime} showLineNumbers>
              {`<p class="box">
  Tady potřebuju dlouhatánský text,
  tak si pomůžu Malým princem.
  <br />
  „Ne,“ řekl malý princ.
  „Hledám přátele. Co to znamená ochočit?“
  <br />
  „Je to něco, na co se moc zapomíná,“
  odpověděla liška.
  „Znamená to vytvořit pouta…“
  …
</p>
`}
            </SyntaxHighlighter>
          </Box>
          <Box>
            <Typography variant="h5">Výsledek</Typography>
            <Box className={basicStyle.result}>
              <ButtonGroup variant="contained" color="primary" size="small" sx={{ marginBottom: '0.5rem' }}>
                <Button
                  onClick={() => setLong(!long)}
                  startIcon={long ? <ZoomInMapIcon /> : <ZoomOutMapIcon />}
                >
                  {long ? 'Zúžit' : 'Prodloužit'}
                </Button>
              </ButtonGroup>
              <style>
                {`
              br {
                display: block;
                margin: 0.25rem 0;
              }

              .box {
                width: ${long ? '90' : '50'}%;
                box-shadow: 0 0 5px 2px rgb(0 0 0 / 0.5);

                overflow: hidden;

                display: -webkit-box;
                -webkit-box-orient: vertical;

                -webkit-line-clamp: 5;
                line-clamp: 5;
              }
              `}
              </style>
              <p className="box">
                Tady potřebuju dlouhatánský text, tak si pomůžu Malým princem.
                <br />
                „Ne,“ řekl malý princ. „Hledám přátele. Co to znamená ochočit?“
                <br />
                „Je to něco, na co se moc zapomíná,“ odpověděla liška. „Znamená to vytvořit pouta…“
                <br />
                „Vytvořit pouta?“
                <br />
                „Ovšem,“ řekla liška. „Ty jsi pro mne jen
                malým chlapcem podobným statisícům malých chlapců.
                Nepotřebuji tě a ty mě také nepotřebuješ.
                Jsem pro tebe jen liškou podobnou statisícům lišek.
                Ale když si mě ochočíš, budeme potřebovat jeden druhého.
                Budeš pro mne jediným na světě a já zase pro tebe jedinou na světě...“
                <br />
                „Začínám chápat,“ řekl malý princ.
                „Znám jednu květinu … myslím, že si mě ochočila…“
              </p>
            </Box>
          </Box>
        </Box>
      </Box>
    </>
  );
};
export default LineClamp;
