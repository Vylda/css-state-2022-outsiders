import { Box, Typography } from '@mui/material';
import clsx from 'clsx';
import { Helmet } from 'react-helmet';
import SyntaxHighlighter from 'react-syntax-highlighter';
import { monokaiSublime } from 'react-syntax-highlighter/dist/esm/styles/hljs';
import { Reference } from 'Components';
import basicStyle from './BasicStyles.module.css';

const ShadowDomPartA = () => (
  <>
    <Helmet>
      <title>Shadow DOM ::part() - Custom element | State of CSS 2022 outsiders</title>
    </Helmet>
    <h1>Shadow DOM ::part() - Custom element</h1>
    <Reference
      canIUseUrl="https://caniuse.com/mdn-api_element_part"
      mdnUrl="https://developer.mozilla.org/en-US/docs/Web/API/Element/part"
      chrome={73}
      edge={79}
      firefox={72}
      opera={60}
      safari={13.1}
      title="Atribut part"
    >
      <p>
        Shadow DOM odstiňuje kód, CSS a elementy Custom komponenty od DOMu.
      </p>
    </Reference>
    <Box>
      <Box className={clsx(basicStyle.code, basicStyle['two-cols'])}>
        <Box>
          <Typography variant="h5">Komponenta: HTML s CSS</Typography>
          <SyntaxHighlighter language="xml" style={monokaiSublime} showLineNumbers>
            {`<template id="small-callendar">
  <style>
    :host {
      display: flex;
      gap: 1rem;
      flex-wrap: wrap;
      justify-content: space-between;
    }

    div[part~=day] {
      box-sizing: content-box;
      padding: 1rem;
      border-bottom: 2px solid transparent;
      cursor: pointer;
      width: 5rem;
      text-align: center;
    }

    div[part~=day]:hover {
      background-color: rgb(230, 231, 240);
    }

    div[part~=today] {
      font-weight: bold;
      border-color: rgba(25, 25, 245, 0.692);
    }
  </style>

  <div part="day day1 today">Pondělí</div>
  <div part="day day2">Úterý</div>
  <div part="day day3">Středa</div>
  <div part="day day4">Čtvrtek</div>
  <div part="day day5">Pátek</div>
  <div part="day day6">Sobota</div>
  <div part="day day0">Neděle</div>
</template>`}
          </SyntaxHighlighter>
        </Box>
        <Box>
          <Typography variant="h5">Komponenta: JS</Typography>
          <SyntaxHighlighter language="javascript" style={monokaiSublime} showLineNumbers>
            {`class SmallCallendar extends HTMLElement {
  #shadow = null;

  constructor() {
    super();
    this.#shadow = this
      .attachShadow({ mode: 'open' });

    const template = document
      .querySelector('#small-callendar');

    this.#shadow
      .appendChild(template.content);
  }

  connectedCallback() {
    const now = new Date();
    const day = now.getDay();

    const oldDayElement = this
      .#shadow
      .querySelector('div[part~=today]');

    oldDayElement.part.remove('today');

    const dayElement = this
      .#shadow
      .querySelector(\`div[part~=day\${day}]\`);

    dayElement.part.add('today');
  }
}`}
          </SyntaxHighlighter>
        </Box>
      </Box>
    </Box>
  </>
);

export default ShadowDomPartA;
