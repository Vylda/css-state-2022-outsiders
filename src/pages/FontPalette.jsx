import { Box, Typography } from '@mui/material';
import clsx from 'clsx';
import { Helmet } from 'react-helmet';
import SyntaxHighlighter from 'react-syntax-highlighter';
import { monokaiSublime } from 'react-syntax-highlighter/dist/esm/styles/hljs';
import { Reference } from 'Components';
import basicStyle from './BasicStyles.module.css';
import style from './FontPalette.module.css';

const FontPalette = () => (
  <>
    <Helmet>
      <title>font-palette | State of CSS 2022 outsiders</title>
    </Helmet>
    <h1>font-palette</h1>
    <Reference
      mdnUrl="https://developer.mozilla.org/en-US/docs/Web/CSS/font-palette"
      canIUseUrl="https://caniuse.com/mdn-css_properties_font-palette"
      chrome={101}
      edge={101}
      firefox={107}
      statsName="font-palette"
      opera={87}
      safari={15.4}
      title="Vlastnost font-palette"
    >
      <p>
        Pomocí vlastnosti
        {' '}
        <code>font-palette</code>
        {' '}
        lze přebarvit barvy v barevném písmu. Přebarvení se nastavuje pomocí pravidla
        {' '}
        <code>@font-palette-values</code>
        {' '}
        a vlastnosti
        {' '}
        <code>override-colors</code>
        .
      </p>
      <p>Takže bychom mohli mít i dvoubarevné ikonky pomocí písma (icon font).</p>
    </Reference>

    <Box>
      <Box className={basicStyle.code}>
        <Box>
          <Typography variant="h5">CSS</Typography>
          <SyntaxHighlighter language="css" style={monokaiSublime} showLineNumbers>
            {`.bungee {
  font-family: "Bungee Spice", cursive;
  font-size: 2rem;
}

@font-palette-values --Red {
  font-family: "Bungee Spice";
  override-colors: 0 #f00, 1 #700;
}

.red {
  font-palette: --Red;
}

@font-palette-values --Green {
  font-family: "Bungee Spice";
  override-colors: 0 #0fb, 1 #074;
}

.green {
  font-palette: --Green;
}

@font-palette-values --Blue {
  font-family: "Bungee Spice";
  override-colors: 0 #0bf, 1 #047;
}

.blue {
  font-palette: --Blue;
}

.icon {
  font-size: 3rem;
}

@font-palette-values --RedIcon {
  font-family: "Material Icons Two Tone";
  override-colors: 1 #f00;
}

.redIcon {
  font-palette: --RedIcon;
  color: #ff0;
}`}
          </SyntaxHighlighter>
        </Box>
        <Box>
          <Typography variant="h5">HTML</Typography>
          <SyntaxHighlighter language="xml" style={monokaiSublime} showLineNumbers>
            {`<p class="bungee">
  Původní barvy
</p>

<p class="bungee red">
  Do červena
</p>

<p class="bungee green">
  Do zelena
</p>

<p class="bungee blue">
  Do modra
</p>

<p>
  <span
    class="icon material-icons-two-tone"
  >
    home
  </span>
  <span
    class="icon redIcon material-icons-two-tone"
  >
    home
  </span>
</p>`}
          </SyntaxHighlighter>
        </Box>
        <Box>
          <Typography variant="h5">Výsledek</Typography>
          <Box className={basicStyle.result}>
            <style>
              {
          `
          @font-palette-values --Red {
            font-family: "Bungee Spice";
            override-colors: 0 #f00, 1 #700;
          }

          @font-palette-values --Green {
            font-family: "Bungee Spice";
            override-colors: 0 #0fb, 1 #074;
          }

          @font-palette-values --Blue {
            font-family: "Bungee Spice";
            override-colors: 0 #0bf, 1 #047;
          }

          .red {
            font-palette: --Red;
          }

          .green {
            font-palette: --Green;
          }

          .blue {
            font-palette: --Blue;
          }

          @font-palette-values --RedIcon {
            font-family: "Material Icons Two Tone";
            override-colors: 1 #f00;
          }

          .redIcon {
            font-palette: --RedIcon;
            color: #ff0;
          }
          `
        }
            </style>
            <p className={style.bungee}>
              Původní barvy
            </p>
            <p className={clsx(style.bungee, 'red')}>
              Do červena
            </p>
            <p className={clsx(style.bungee, 'green')}>
              Do zelena
            </p>
            <p className={clsx(style.bungee, 'blue')}>
              Do modra
            </p>
            <p>
              <span className={clsx(style.icon, 'material-icons-two-tone')}>
                home
              </span>
              <span className={clsx(style.icon, 'redIcon', 'material-icons-two-tone')}>
                home
              </span>
            </p>
          </Box>
        </Box>
      </Box>
    </Box>
  </>
);

export default FontPalette;
