import {
  Box, ButtonGroup, Typography,
} from '@mui/material';
import clsx from 'clsx';
import { useState } from 'react';
import { Helmet } from 'react-helmet';
import SyntaxHighlighter from 'react-syntax-highlighter';
import { monokaiSublime } from 'react-syntax-highlighter/dist/esm/styles/hljs';
import { Reference, CodeButton } from 'Components';
import basicStyle from './BasicStyles.module.css';

const CssLogicalProps = () => {
  const [isRl, setIsRl] = useState(true);
  return (
    <>
      <Helmet>
        <title>CSS logical properties | State of CSS 2022 outsiders</title>
      </Helmet>
      <h1>CSS logical properties</h1>
      <Reference
        canIUseUrl="https://caniuse.com/css-logical-props"
        mdnUrl="https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Logical_Properties"
        chrome={89}
        edge={89}
        firefox={66}
        statsName="CSS logical properties"
        opera={76}
        safari={15}
        title="CSS logical properties"
      >
        <p>
          Patří sem vlastnosti pro rozměry, marginy, okraje,
          paddingy, pozicování, obtékání, velikost kontejneru a další (celkem 69).
        </p>
        <p>
          Některé hodnoty už používaných vlastností
          (třeba
          {' '}
          <code>block</code>
          {' '}
          a
          {' '}
          <code>inline</code>
          {' '}
          u vlastnosti
          {' '}
          <code>resize</code>
          ) nejsou všude podporovány.
        </p>
        <p>
          Souvisí zejména s tokem textu.
        </p>
      </Reference>
      <Box>
        <Box className={basicStyle.code}>
          <Box>
            <Typography variant="h5">CSS</Typography>
            <SyntaxHighlighter language="css" style={monokaiSublime} showLineNumbers>
              {`/* nějaké ty formátovací styly nevypisuju */

.rtl {
  direction: rtl;
}

.rl {
  writing-mode: vertical-rl;
}

.end {
  text-align: end;
}

.inset {
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;

  inset-block: 10px 50px;
  inset-inline: 30px 70px;

  border-block: dotted;
  border-block-color: black;
  border-block-width: 1rem;
}`}
            </SyntaxHighlighter>
          </Box>
          <Box>
            <Typography variant="h5">HTML</Typography>
            <SyntaxHighlighter language="xml" style={monokaiSublime} showLineNumbers>
              {`<p class="end" title="ltr">
  Zarovnávám na konec!
</p>

<p class="end rtl" title="rtl">
  cenok an mávánvoraZ!
</p>

<div>
  Tady je nějaký text, takže je to tady nějaké popsané.
  Ne postříkané, ani ne posprejované. Prostě popsané
  …
  <div class="${isRl ? 'rl ' : ''}inset">
      Tohle je absolutně pozicovaný blok
      s&nbsp;insetem a borderem
  </div>
</div>
`}
            </SyntaxHighlighter>
          </Box>
          <Box>
            <Typography variant="h5">Výsledek</Typography>
            <Box className={basicStyle.result}>
              <style>
                {`
              .boxed {
                box-sizing: border-box;
                border: 1px solid black;
                padding: 0.5rem;
              }

              .relative {
                position: relative;
              }

              .absolute {
                position: absolute;
              }

              .outerDiv {
                background-color: black;
                color: white;
                width: 300px;
                aspect-ratio: 3 / 2;
                padding: 0.25rem;
              }

              .innerDiv {
                background-color: rgb(220 220 60 / 80%);
                color: black;
                padding: 0.25rem;
              }

              .rtl {
                direction: rtl;
              }

              .rl {
                writing-mode: vertical-rl;
              }

              .end {
                text-align: end;
              }

              .inset {
                top: 0;
                bottom: 0;
                left: 0;
                right: 0;
                inset-block: 10px 50px;
                inset-inline: 30px 70px;
                border-block: dotted;
                border-block-color: black;
                border-block-width: 1rem;
              }
              `}
              </style>

              <p className="end boxed" title="ltr">
                Zarovnávám na konec!
              </p>
              <p className="end rtl boxed" title="rtl">
                cenok an mávánvoraZ!
              </p>

              <ButtonGroup
                variant="contained"
                color="primary"
                size="small"
                sx={{ marginBottom: '0.5rem', marginTop: '1rem' }}
              >
                <CodeButton
                  showStartIcon={isRl}
                  onClick={() => setIsRl(true)}
                >
                  je RL
                </CodeButton>
                <CodeButton
                  showStartIcon={!isRl}
                  onClick={() => setIsRl(false)}
                >
                  není RL
                </CodeButton>
              </ButtonGroup>

              <div className="outerDiv relative">
                Tady je nějaký text, takže je to tady nějaké popsané.
                Ne postříkané, ani ne posprejované. Prostě popsané
                písmenky neboli litarami. A proto to tu psal asi nějaký literát.
                Je to tedy literatura? Vždyť přeci v každé literatuře se vyskytují litery.
                Ale to je chybný úsudek - argumentační faul.
                <div className={clsx(isRl && 'rl', 'innerDiv', 'absolute', 'inset')}>
                  Tohle je absolutně pozicovaný blok s&nbsp;insetem a borderem
                </div>
              </div>

            </Box>
          </Box>
        </Box>
      </Box>
    </>
  );
};

export default CssLogicalProps;
