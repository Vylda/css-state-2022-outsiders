import { Box, Typography } from '@mui/material';
import { useEffect, useRef, useState } from 'react';
import { Helmet } from 'react-helmet';
import SyntaxHighlighter from 'react-syntax-highlighter';
import { monokaiSublime } from 'react-syntax-highlighter/dist/esm/styles/hljs';
import { Reference, ValuesList } from 'Components';
import basicStyle from './BasicStyles.module.css';

const AccentColor = () => {
  const [chbState, setChbState] = useState(false);
  const [rbState, setRbState] = useState(false);
  const [rgState, setRgState] = useState(42);
  const [prgState, setPrgState] = useState(42);
  const prgStateRef = useRef(42);

  useEffect(() => {
    setInterval(() => {
      const newValue = prgStateRef.current + 1;
      if (newValue > 100) {
        prgStateRef.current = 0;
        setPrgState(0);
        return;
      }

      prgStateRef.current = newValue;
      setPrgState(newValue);
    }, 100);
  }, []);

  return (
    <>
      <Helmet>
        <title>accent-color | State of CSS 2022 outsiders</title>
      </Helmet>
      <h1>accent-color</h1>
      <Reference
        canIUseUrl="https://caniuse.com/mdn-css_properties_accent-color"
        mdnUrl="https://developer.mozilla.org/en-US/docs/Web/CSS/accent-color"
        chrome={93}
        edge={93}
        firefox={92}
        statsName="accent-color"
        opera={79}
        safari={15.4}
        title="Vlastnost accent-color"
      >
        <p>
          Nastaví barvu elementům, které jsou stylovány browserem.
          Jde hlavně o formulářové prvky. Moc jich teda není.
        </p>
        <ValuesList
          title="Podporované elementy:"
          list={[
            {
              id: 'checkbox',
              primary: '<input type="checkbox">',
            },
            {
              id: 'radio',
              primary: '<input type="radio">',
            },
            {
              id: 'range',
              primary: '<input type="range">',
            },
            {
              id: 'progress',
              primary: '<progress>',
            },
          ]}
        />
      </Reference>
      <Box>
        <Box className={basicStyle.code}>
          <Box>
            <Typography variant="h5">CSS</Typography>
            <SyntaxHighlighter language="css" style={monokaiSublime} showLineNumbers>
              {`/* nějaké ty formátovací styly nevypisuju */

.checkbox {
  accent-color: rgb(162 18 18);
}

.radio {
  accent-color: rgb(66 200 17);
}

.range {
  accent-color: rgb(247 127 21);
}

.progress {
  accent-color: rgb(154 7 196);
}`}
            </SyntaxHighlighter>
          </Box>
          <Box>
            <Typography variant="h5">HTML</Typography>
            <SyntaxHighlighter language="xml" style={monokaiSublime} showLineNumbers>
              {`<form>

  <label for="checkbox">
    <input
      type="checkbox"
      id="checkbox"
      className="checkbox"
    /> Zatržítko
  </label>

  <label for="radio">
    <input
      type="radio"
      id="radio"
      class="radio"
    /> Volítko
  </label>

  <input
    type="range"
    id="range"
    class="range"
    min="0"
    max="100"
    value="42"
    step="2"
  />

  <progress
    class="progress"
    max="100"
    value="42"
  >
    42 %
  </progress>

</form>
`}
            </SyntaxHighlighter>
          </Box>
          <Box>
            <Typography variant="h5">Výsledek</Typography>
            <Box className={basicStyle.result}>
              <style>
                {`.form {
                width: 100%;
                transform: scale(150%);
                transform-origin: top left;
              }

              .element {
                display: block;
                margin-bottom: 0.5rem;
              }

              .checkbox {
                accent-color: rgb(162 18 18);
              }

              .radio {
                accent-color: rgb(66 200 17);
              }

              .range {
                accent-color: rgb(247 127 21);
              }

              .progress {
                accent-color: rgb(154 7 196);
              }
              `}
              </style>
              <form className="form">
                <label htmlFor="checkbox" className="element">
                  <input
                    type="checkbox"
                    id="checkbox"
                    checked={chbState}
                    className="checkbox"
                    onChange={() => setChbState(!chbState)}
                  />
                  {' '}
                  Zatržítko
                </label>
                <label htmlFor="radio" className="element">
                  <input
                    type="radio"
                    id="radio"
                    checked={rbState}
                    onClick={() => setRbState(!rbState)}
                    readOnly
                    className="radio"
                  />
                  {' '}
                  Volítko
                </label>
                <input
                  type="range"
                  id="range"
                  className="element range"
                  min="0"
                  max="100"
                  value={rgState}
                  onChange={(e) => setRgState(e.target.value)}
                  step="2"
                />
                <progress className="element progress" max="100" value={prgState}>{`${prgState} %`}</progress>
              </form>
            </Box>
          </Box>
        </Box>
      </Box>
    </>
  );
};
export default AccentColor;
