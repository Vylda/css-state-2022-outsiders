import {
  Box, ButtonGroup, Typography,
} from '@mui/material';
import clsx from 'clsx';
import { useState } from 'react';
import { Helmet } from 'react-helmet';
import SyntaxHighlighter from 'react-syntax-highlighter';
import { monokaiSublime } from 'react-syntax-highlighter/dist/esm/styles/hljs';
import { CodeButton, Reference, ValuesList } from 'Components';
import basicStyle from './BasicStyles.module.css';

const ColorScheme = () => {
  const [elementClass, setElementClass] = useState('normal');

  return (
    <>
      <Helmet>
        <title>color-scheme | State of CSS 2022 outsiders</title>
      </Helmet>
      <h1>color-scheme</h1>
      <Reference
        canIUseUrl="https://caniuse.com/mdn-css_properties_color-scheme"
        mdnUrl="https://developer.mozilla.org/en-US/docs/Web/CSS/color-scheme"
        chrome={81}
        edge={81}
        firefox={96}
        statsName="color-scheme"
        opera={68}
        safari={13}
        title="Vlastnost color-scheme"
      >
        <p>
          Slouží k nastavení světlého nebo tmavého režimu
          buď celé stránky, nebo konkrétních elementů.
        </p>
        <p>
          Obvykle se nastavuje pro
          {' '}
          <code>:root</code>
          .
        </p>
        <p>
          Mění se defaultní barva textu a barva pozadí stránky,
          standardní formulářové prvky, scrollbary a systémové barvy.
        </p>
        <ValuesList list={[
          {
            id: 'normal',
            primary: 'normal',
            secondary: 'Nereaguje na změnu režimu.',
          },
          {
            id: 'light',
            primary: 'light',
            secondary: 'Prvek (stránka) je ve světlém režimu.',
          },
          {
            id: 'dark',
            primary: 'dark',
            secondary: 'Prvek (stránka) je v tmavém režimu.',
          },
          {
            id: 'light dark',
            primary: 'light dark',
            secondary: 'Prvek (stránka) je v tmavém nebo světlém režimu podle nastavení prohlížeče.',
          },
        ]}
        />
      </Reference>
      <Box>
        <Box className={basicStyle.code}>
          <Box>
            <Typography variant="h5">CSS</Typography>
            <SyntaxHighlighter language="css" style={monokaiSublime} showLineNumbers>
              {`/* nějaké ty formátovací styly nevypisuju */

.normal {
  color-scheme: normal;
}

.light {
  color-scheme: light;
}

.dark {
  color-scheme: dark;
}

.light-dark {
  color-scheme: light dark;
}
`}
            </SyntaxHighlighter>
          </Box>
          <Box>
            <Typography variant="h5">HTML</Typography>
            <SyntaxHighlighter language="xml" style={monokaiSublime} showLineNumbers>
              {`<textarea class="${elementClass}">
  Textarea
</textarea>`}
            </SyntaxHighlighter>
          </Box>
          <Box>
            <Typography variant="h5">Výsledek</Typography>
            <Box className={basicStyle.result}>
              <ButtonGroup variant="contained" color="primary" size="small">
                <CodeButton
                  showStartIcon={elementClass === 'normal'}
                  onClick={() => setElementClass('normal')}
                >
                  normal
                </CodeButton>
                <CodeButton
                  showStartIcon={elementClass === 'light'}
                  onClick={() => setElementClass('light')}
                >
                  light
                </CodeButton>
                <CodeButton
                  showStartIcon={elementClass === 'dark'}
                  onClick={() => setElementClass('dark')}
                >
                  dark
                </CodeButton>
                <CodeButton
                  showStartIcon={elementClass === 'light-dark'}
                  onClick={() => setElementClass('light-dark')}
                >
                  light-dark
                </CodeButton>
              </ButtonGroup>
              <br />
              <style>
                {`.area {
                width: 50%;
                height: 5rem;
                margin-top: 1rem;
              }

              .normal {
                  color-scheme: normal;
                }

                .light {
                  color-scheme: light;
                }

                .dark {
                  color-scheme: dark;
                }

                .light-dark {
                  color-scheme: light dark;
                }`}
              </style>
              <textarea className={clsx(elementClass, 'area')} defaultValue="Textarea" />
            </Box>
          </Box>
        </Box>
      </Box>
    </>
  );
};

export default ColorScheme;
