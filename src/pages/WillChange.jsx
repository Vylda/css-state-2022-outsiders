import {
  Box, ButtonGroup, Typography,
} from '@mui/material';
import clsx from 'clsx';
import { useMemo, useState } from 'react';
import { Helmet } from 'react-helmet';
import SyntaxHighlighter from 'react-syntax-highlighter';
import { monokaiSublime } from 'react-syntax-highlighter/dist/esm/styles/hljs';
import { Reference, CodeButton } from 'Components';
import basicStyle from './BasicStyles.module.css';

const perLine = 10;

const WillChange = () => {
  const willChangeState = {
    none: 0,
    elements: 1,
  };
  const [willChange, setWillChange] = useState(willChangeState.none);

  const divs = useMemo(
    () => Array.from(Array(perLine)
      .keys())
      .map((divKey) => (
        <div
          className={clsx(
            'box',
            willChange === willChangeState.elements && 'will-change',
          )}
          key={divKey}
        >
          Vyjížděcí box
        </div>
      )),
    [willChange],
  );

  return (
    <>
      <Helmet>
        <title>will-change | State of CSS 2022 outsiders</title>
      </Helmet>
      <h1>will-change</h1>
      <Reference
        canIUseUrl="https://caniuse.com/will-change"
        mdnUrl="https://developer.mozilla.org/en-US/docs/Web/CSS/will-change"
        chrome={36}
        edge={79}
        firefox={36}
        statsName="will-change"
        opera={24}
        safari={9.1}
        title="Vlastnost will-change"
      >
        <p>
          Slouží k optimalizaci před tím, než se element bude měnit - animovat.
          No evidentně ne na veškeré animace. Spolehlivě to funguje na
          {' '}
          <code>will-change: transform</code>
          {' '}
          a při
          {' '}
          <code>transition</code>
          .
        </p>
        <p>
          Mělo by to přepnout rendering do GPU (podle některých článků).
        </p>
        <p>
          V Chromu: Rendering ⇒ Paint flashing.
        </p>
      </Reference>
      <Box>
        <Box className={basicStyle.code}>
          <Box>
            <Typography variant="h5">CSS</Typography>
            <SyntaxHighlighter language="css" style={monokaiSublime} showLineNumbers>
              {`/* nějaké ty formátovací styly nevypisuju */

.box {
  width: 10rem;
  height: 1.5rem;
  padding: 0.75rem;
  text-align: center;
  border: 1px solid black;
  background-color: rgb(100 190 240 / 0.75);
  color: white;
  cursor: pointer;
  position: relative;
  transition: 0.3s;
  transform: translateX(-8rem) rotate(0);
}

.box:hover {
  transform: translateX(0) rotate(-5deg);
}

.box.will-change {
  will-change: transform;
}`}
            </SyntaxHighlighter>
          </Box>
          <Box>
            <Typography variant="h5">HTML</Typography>
            <SyntaxHighlighter language="xml" style={monokaiSublime} showLineNumbers>
              {`<div>
  <div class="${willChange === willChangeState.elements ? 'box will-change' : 'box'}">
    Vyjížděcí box
  </div>}
  .
  .
  .
  <div class="${willChange === willChangeState.elements ? 'box will-change' : 'box'}">
    Vyjížděcí box
  </div>}
</div>`}
            </SyntaxHighlighter>
          </Box>
          <Box>
            <Typography variant="h5">Výsledek</Typography>
            <Box className={basicStyle.result}>
              <ButtonGroup variant="contained" color="primary" size="small" sx={{ marginBottom: '0.5rem' }}>
                <CodeButton
                  showStartIcon={willChange === willChangeState.none}
                  onClick={() => setWillChange(willChangeState.none)}
                >
                  will-change nikde
                </CodeButton>
                <CodeButton
                  showStartIcon={willChange === willChangeState.elements}
                  onClick={() => setWillChange(willChangeState.elements)}
                >
                  will-change na elementech
                </CodeButton>
              </ButtonGroup>
              <style>
                {`
              .grid {
                display: grid;
                gap: 0.25rem;
                grid-template-rows: repeat(${perLine}, 1fr);
                width: 15rem;
                border: 1px solid black;
                overflow: hidden;
              }

              .box {
                width: 10rem;
                height: 1.5rem;
                padding: 0.75rem;
                text-align: center;
                border: 1px solid black;
                background-color: rgb(100 190 240 / 0.75);
                color: white;
                cursor: pointer;
                position: relative;
                transition: 0.3s;
                transform: translateX(-8rem) rotate(0);
              }

              .box:hover {
                transform: translateX(0) rotate(-5deg);
              }

              .box.will-change {
                will-change: transform;
              }
              `}
              </style>
              <div className="grid">
                {divs}
              </div>
            </Box>
          </Box>
        </Box>
      </Box>
    </>
  );
};

export default WillChange;
