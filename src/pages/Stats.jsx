import {
  AllInclusiveRounded, BuildRounded, HelpRounded, SlideshowRounded, WebRounded,
} from '@mui/icons-material';
import {
  BottomNavigation, BottomNavigationAction, Box,
} from '@mui/material';
import { Helmet } from 'react-helmet';
import {
  BarChart, Bar, XAxis, YAxis, Tooltip, ResponsiveContainer,
} from 'recharts';
import CustomChartTooltip from 'Components/CustomChartTooltip/CustomChartTooltip';
import { dataFormatter, tickFormatter } from 'Utils/formatters';
import useStats from './hooks/useStats';
import style from './Stats.module.css';

const BAR_WIDTH = 20;
const FIELD_WIDTH = 2 * BAR_WIDTH;

const Stats = () => {
  // eslint-disable-next-line no-unused-vars
  const {
    data, dataState, onChange, title, subtitle,
  } = useStats();

  return (
    <>
      <Helmet>
        <title>Statistiky | State of CSS 2022 outsiders</title>
      </Helmet>
      <h1>Statistiky</h1>
      <Box>
        <h2 className={style['subtitle-heading']}>
          {title}
        </h2>
        <h3 className={style.subtitle}>
          {subtitle}
        </h3>
      </Box>
      <Box>
        <BottomNavigation
          showLabels
          value={dataState}
          onChange={onChange}
          className={style.switchbox}
        >
          <BottomNavigationAction label="Vše" value="all" icon={<AllInclusiveRounded />} />
          <BottomNavigationAction label="Podle používání" value="used" icon={<BuildRounded />} />
          <BottomNavigationAction label="Podle nevědomostí" value="byIDN" icon={<HelpRounded />} />
          <BottomNavigationAction label="Podporované neznalé" value="justWithFullSupport" icon={<WebRounded />} />
          <BottomNavigationAction label="Co si ukážeme" value="supportedSelect" icon={<SlideshowRounded />} />
        </BottomNavigation>
      </Box>
      <ResponsiveContainer width="100%" height={data.length * FIELD_WIDTH}>
        <BarChart
          width={600}
          height={data.length * FIELD_WIDTH}
          data={data}
          layout="vertical"
          barCategoryGap={BAR_WIDTH / 3}
          margin={{
            top: 20,
            right: 100,
            bottom: 20,
            left: 150,
          }}
        >
          <XAxis type="number" orientation="top" tickFormatter={dataFormatter} />
          <YAxis
            dataKey="name"
            type="category"
            scale="auto"
            allowDataOverflow
            tickFormatter={tickFormatter}
          />
          <Tooltip content={<CustomChartTooltip />} />
          <Bar dataKey="Neznám to" fill="#a20c0c" stackId="a" />
          <Bar dataKey="Slyšel jsem" fill="#3067e8" stackId="a" />
          <Bar dataKey="Použil jsem" fill="#118f06" stackId="a" />
        </BarChart>
      </ResponsiveContainer>
    </>
  );
};

export default Stats;
