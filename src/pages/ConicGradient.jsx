import { Box, Typography } from '@mui/material';
import { Helmet } from 'react-helmet';
import SyntaxHighlighter from 'react-syntax-highlighter';
import { monokaiSublime } from 'react-syntax-highlighter/dist/esm/styles/hljs';
import { Reference } from 'Components';
import basicStyle from './BasicStyles.module.css';

const ConicGradient = () => (
  <>
    <Helmet>
      <title>conic-gradient() | State of CSS 2022 outsiders</title>
    </Helmet>
    <h1>conic-gradient()</h1>
    <Reference
      canIUseUrl="https://caniuse.com/mdn-css_types_image_gradient_conic-gradient"
      mdnUrl="https://developer.mozilla.org/en-US/docs/Web/CSS/gradient/conic-gradient"
      chrome={69}
      edge={79}
      firefox={83}
      statsName="conic-gradient()"
      opera={56}
      safari={12.1}
      title="Funkce conic-gradient()"
    />
    <Box>
      <Box className={basicStyle.code}>
        <Box>
          <Typography variant="h5">CSS</Typography>
          <SyntaxHighlighter language="css" style={monokaiSublime} showLineNumbers>
            {`.gradient {
  width: 15rem;
  aspect-ratio: 1 / 1;
  border-radius: 50%;

  background-image: conic-gradient(
      hsl(360 100% 50%),
      hsl(315 100% 50%),
      hsl(270 100% 50%),
      hsl(225 100% 50%),
      hsl(180 100% 50%),
      hsl(135 100% 50%),
      hsl(90 100% 50%),
      hsl(45 100% 50%),
      hsl(0 100% 50%)
  );
}
`}
          </SyntaxHighlighter>
        </Box>
        <Box>
          <Typography variant="h5">HTML</Typography>
          <SyntaxHighlighter language="xml" style={monokaiSublime} showLineNumbers>
            {'<div class="gradient" />'}
          </SyntaxHighlighter>
        </Box>
        <Box>
          <Typography variant="h5">Výsledek</Typography>
          <Box className={basicStyle.result}>
            <style>
              {`
            .gradient {
              width: 15rem;
              aspect-ratio: 1 / 1;
              background-image: conic-gradient(
                  hsl(360 100% 50%),
                  hsl(315 100% 50%),
                  hsl(270 100% 50%),
                  hsl(225 100% 50%),
                  hsl(180 100% 50%),
                  hsl(135 100% 50%),
                  hsl(90 100% 50%),
                  hsl(45 100% 50%),
                  hsl(0 100% 50%)
              );
              border-radius: 50%;
            }
            `}
            </style>
            <div className="gradient" />
          </Box>
        </Box>
      </Box>
    </Box>
  </>
);

export default ConicGradient;
