import useKey from '@rooks/use-key';
import {
  useCallback, useLayoutEffect, useMemo, useRef, useState,
} from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import Index from 'Pages/Index';
import pages from 'Pages/pages';

const resolvePath = (path) => {
  if (!path) {
    return undefined;
  }

  return path.startsWith('/') ? path : `/${path}`;
};

const useApp = () => {
  const { pathname } = useLocation();
  const navigate = useNavigate();
  const [links, setLinks] = useState({
    next: undefined,
    prev: undefined,
  });
  const waitTimer = useRef(undefined);

  const navigateByKeyboard = useCallback((direction) => {
    if (waitTimer.current) {
      return;
    }

    waitTimer.current = setTimeout(() => {
      waitTimer.current = undefined;
    }, 1000);

    const { prev, next } = links;
    switch (direction) {
      case 'next':
        if (next) {
          navigate(next);
        }
        break;
      case 'prev':
        if (prev) {
          navigate(prev);
        }
        break;
      default:
    }
  }, [links, navigate]);

  useKey(['ArrowRight', ' ', 'PageDown'], () => navigateByKeyboard('next'));
  useKey(['Backspace', 'ArrowLeft', 'PageUp'], () => navigateByKeyboard('prev'));

  const Wrapper = ({ children }) => {
    useLayoutEffect(() => {
      document?.querySelector('main')?.scrollTo(0, 0);
    }, [pathname]);

    return children;
  };

  const Component = useMemo(() => {
    const componentItemIndex = pages.findIndex((item) => (resolvePath(item.path) === pathname));
    const prev = resolvePath(pages[componentItemIndex - 1]?.path);
    const next = resolvePath(pages[componentItemIndex + 1]?.path);

    setLinks({ prev, next });

    if (componentItemIndex >= 0) {
      return <Wrapper>{pages[componentItemIndex].component}</Wrapper>;
    }

    return <Wrapper><Index /></Wrapper>;
  }, [pathname]);

  return { Component, pathname, links };
};

export default useApp;
