import { Alert, AlertTitle } from '@mui/material';
import { Suspense } from 'react';
import { Helmet } from 'react-helmet';
import { Footer, Loader } from 'Components';
import MainMenu from 'Components/MainMenu/MainMenu';
import style from './App.module.css';
import useApp from './hooks/useApp';

const App = () => {
  const { Component, pathname, links } = useApp();

  return (
    <div className={style.app}>
      <Helmet>
        <link rel="icon" type="image/svg+xml" href="/favicon.svg" />
      </Helmet>
      <header className={style.header}>
        <MainMenu path={pathname} />
      </header>
      <Suspense fallback={<Loader />}>
        <main className={style.main}>
          {Component}
        </main>
        <Alert severity="error" className={style.alert}>
          <AlertTitle>Sorry!</AlertTitle>
          Ale na takhle malou obrazovku to nedám. Potřebuju aspoň 761 pixelů na šířku!
        </Alert>
      </Suspense>
      <Footer prev={links.prev} next={links.next} />
    </div>
  );
};

export default App;
