export const dataFormatter = (number) => {
  const options = {
    style: 'percent',
    minimumFractionDigits: 1,
  };

  return Intl.NumberFormat('cs', options).format(number);
};

export const tickFormatter = (category) => category.replace(/\s+/g, ' ');
